import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BasketsService, MiamRecipe, ContextService, RecipesService, Recipe } from 'ng-miam';
import { Icon } from 'projects/ng-miam/src/lib/_types/icon.enum';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  recipe1: Recipe;
  context = { shelfIngredients: ['0400958'], currentIngredients: [], basketIngredients: ['1387616'] };
  emptyContext = { shelfIngredients: [], currentIngredients: [], basketIngredients: [] };
  recipe: MiamRecipe = {
    title: 'Cake jambon-olives',
    source: '',
    'cooking-time': '5',
    'media-url': 'https://storage.googleapis.com/images.miam.tech/recettes/cake-aux-olives-vertes-mimolette-et-jambon-375.png',
    'number-of-guests': '4',
    'preheating-time': '5',
    'preparation-time': '6',
    'resting-time': '7',
    'ingredients-str': ['10cL d\'huile olive', '2 tranches de jambon blanc', '150g farine', 'olives vertes'],
    'ext-link': ''
  };
  randomRecipe: Recipe;
  basketStats = {};
  icon = Icon;
  icons = Object.keys(this.icon).filter(i => Number.isNaN(+i));
  placholderIngredient = 'Ajouter des ingrédients ou copier une liste depuis le presse papier';
  listIngredients =  ['carre agneau (6 pièces)', 'sel fin (2 pincées)', 'huile d\'olive (15 cL)', 'chapelure (300 g)',
    'sucre semoule (10 g)', 'carotte (12 pièces)', 'ciboulette (1 BotteUnite)'];

  constructor(
    private contextService: ContextService,
    public recipesService: RecipesService,
    public basketsService: BasketsService,
    private router: Router
  ) {
    contextService.miam.analytics.init('test-ga-key');
    contextService.miam.pos.load('393');
    // Load user with its external id, telling API to ignore profiling functions
    // contextService.miam.user.loadWithExtId('test-dev-1', true).subscribe();

    this.prepareRecipeDisplay();
  }

  ngOnInit(): void {
    this.recipesService.getRandom().subscribe(res => {
      this.randomRecipe = res[0];
    });
    this.contextService.miam.user.loadWithExtId('Your token');
    // this.contextService.miam.recipes.provider.load('cora').subscribe();
    this.contextService.miam.supplier.load(3);
    // this.contextService.miam.user.setToken('').subscribe();
    // this.contextService.miam.overrideIcon( 'Heart',  './assets/heart.svg');
  }
  onListReady(list) {
    console.log('LIST UPDATED', list);
  }

  onError(error) {
    console.error(error);
  }

  onLog(event) {
    console.log(event);
  }

  confirmBasket() {
    this.contextService.miam.basket.confirm().subscribe(basket => {
      console.log('basket confirmed with token:', basket.attributes.token);
    });
  }

  payBasket() {
    this.contextService.miam.basket.paid(this.basketStats['totalPrice']);
  }

  private prepareRecipeDisplay() {
    // When recipe to display appears in URL, trigger the modal
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd && event.url.includes('displayRecipe=')) {
        const matcher = event.url.match(/displayRecipe=(\d+)/);
        const recipeId = matcher && matcher[1];
        if (recipeId) {
          this.recipesService.display(recipeId, {});
          // Other way : (window as any).miam.recipes.display(recipeId);
        }
      }
    });

    // When the modal is hidden, redirect to home
    // other way : (window as any).miam.recipes.hidden.subscribe(function(...
    this.recipesService.hidden.subscribe(_event => {
      this.router.navigate(['/']);
    });
  }
}
