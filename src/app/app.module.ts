import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MIAM_PROVIDER_ID, MIAM_STATUS_ID, NgMiamModule } from 'ng-miam';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgMiamModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([{ path: '**', component: AppComponent }])
  ],
  providers: [
    { provide: MIAM_STATUS_ID, useValue: "2" },
    { provide: MIAM_PROVIDER_ID, useValue: "3" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
