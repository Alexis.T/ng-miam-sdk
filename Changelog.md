## v2.25.2
- Fix double pricing requests
- Fix personal recipes CTAs on favorite page
- Change times format on details

## v2.25.1
- set and use hook callback

## v2.25.0
- add package category in catalog
- add filigrane logo and custom sentence

## v2.24.5
- Fix steps long word and pft capacity
- Fix form update

## v2.24.4
- Fix personal no image dropped & description length

## v2.24.3
- Fix image upload button not clickable

## v2.24.2
- Fixes on personal recipes
- Fix pricing not updating on details
- Fix guests not updating correctly after being changed in preview-block

## v2.24.1
- mark recipe type as dirty on change
- fix change detection 
- css fixes
- remove suggested and popularity from recipe

## v2.24.0
- Add personal recipes page
- More actions button on catalog cards for personal recipes
- Card 'Create recipe' & navigation for personal recipes
- add recipe stepper form
- new component recipe type chooser

## v2.23.5
- Add spans on texts when parent has other children
- remove brand of basket prewiew line if null

## v2.23.4
- Add display recipe detail with no preview 
- Fix counter-input accepting numbers and add debounce
- Add include recipe-type on suggestions calls

## v2.23.3
- fix price decimal on replace item
- Add input on recipe helper to change recap location

## v2.23.2
- fix profiling consent
- fix zero price make crash

## v2.23.1
- CatalogCategory url parameter to catalogCategory

## v2.23.0
- Divide prices in two parts to allow different stylings on integer and decimal parts
- select category from url

## v2.22.2
- Fix Catalog page overflow

## v2.22.1
- Fix - loader under catalog list
- Never show recipes in favorite page header 
- Change favorite page title when user isn't connected

## v2.22.0
- Reusable filters component
- Update file input design
- Update list input design
- Update counter input style
- Filters recap in catalog
- Add like button on recipe-cards and detail
- Hide like-button when user isn't logged
- Favorite page
- add a recipe card from recipe Id

## v2.21.2
- Add events (analytics & miam) related to catalog recipes

## v2.21.1
- Fix 404 when going from sponsor page in details to details of recipe without sponsor

## v2.21.0
- Fix recipe ingredient update
- Add sponsor logo to catalog card
- Rework text Input style
- Add a custom select component
- Reduce lodash imports
- Add new script stats:webc
- Replace moment by date-fns and duration-fns
- Return to top button on catalog
- Remove Cost on recipe details
- Update cost filter on catalog to use the new computed_cost filter

## v2.20.2
- add sponsored category

## v2.20.1
- Fix css of collaps filter
- Fix css overlay picture on print

## v2.20.0
- Filter are now collapsable
- Rework time input
- Add new global css classes and properties
- Extract global css from bundle for webc and ng
- Fix picture on print mode on chrome
- Fix : category not scrollable when not enough recipes
- Add lazy loading on recipe cards
- Update slider input 

## v2.19.9
- Fix: pricing requests on each basket update
- Fix basket.totalPrice

## v2.19.8
- Fix: add basket.attributes['total-price'] accessor

## v2.19.7
- Fix: NaN on ingredient quantity
- Fix: catalog list won't load infinitely if anchor is above the screen

## v2.19.6
- Fix guest counter not displaying
- Fix catalog list not auto loading correctly
- Scroll to top of catalog when user clicks on a category

## v2.19.5
- Fix: catalog counter inputs now updates guests for the basket
- Minor fix : request move from constructor to onInit in catalogs
- Fix: catgory slider hiding 1st recipe title
- Fix: filters added both results if a request was made during another
- Fix: catalog cards not updated when removed from list
- Fix: load basket subscription trigger twice. refresh list and take 1 instead

## v2.19.4
- Minor Fix : recipe printing, add some guarding here and there + basket confirmation
- display : Suggestion of the week category,  message when no recipe were found in catalog, sponsor logo to recipe-details 
- Return to categories when no filter or search

## v2.19.3
- Fix: categories not loading on page reload
- catalog header changes image when clicking on a category
- Search button more visible
- Fix error caused by capitalize pipe when string is empty
- Fix: cart icon not updating on catalog-recipe-card

## v2.19.2
- Fix: recipe-types-ids aren't hard-coded anymore in catalog
- Fix: all words started with a capital letter, now only the 1st

## v2.19.1
- ContextService basket.removeEntries to return an observable

## v2.19.0
- Fix: contextService.basket.entries$ to return only active entries
- Refactor: move to userService some context function 
- Add catalog page
- Add new recipe card component for catalog
- Add recipes label component to display references to recipes on basket
- Add print feature on recipe
- Add contextService.basket.removeEntries([])
- Change: rename recipe-detail-modal into recipe-modal

## v2.18.3
- Support sponsored recipes

## v2.18.2
- Fix recipe price when recipe already in basket (show price as per basket preview)

## v2.18.1
- Fix basketService.basketEntries() not returning basket entries any more

## v2.18.0
- Optimised basket loading process
- Add RecipesService.getPricing() & RecipePricing model
- Add RecipePricingComponent to display recipe card & recipe details
- Update recipe details design & add additional CTA at the bottom of the modal
- Add supplier webhook call method to SDK (and add token attribute to basket)
- send suggestion events

**breaking change on recipe details action & price css classes (fix naming & refactor)**

## v2.17.2
- unsuscribe from previous groceries_list calls to avoid race concurrencies

## v2.17.1
- change groceries list current route token

## v2.17.0
- Analytics service to be activated manually by client app
- Client app can forbid profiling when loading user context
- Add an event on recipe-card to know when it's hidden

## v2.16.9
- Fix total not being sent in cents

## v2.16.8
- Fix analytics and add logging on payment event

## v2.16.7
- force refresh current list request

## v2.16.6
- add few logs

## v2.16.5
- Fix analytics ID

## v2.16.4
- Fix analytics ID

## v2.16.3
- Fix analytics configuration

## v2.16.2
- Fix basket observable trigger after confirm
- Display required recipe after pos selection

## v2.16.1
- Fix _miam/listId kept in store while user logged in

## v2.16.0
- Add recipe selection log model and service
- Show recipes history related to logged in user
- Add pagination to recipes history
- Fix event propagation between basket preview line and delete button

**Depends on miam-api v3.48.0**

## v2.15.1
- Fix miam.user : add reset() & avoid double caching of user id or token

## v2.15.0
- Fix size of recipe helper popin
- Leverage backend current list mechanisms instead of creating/reseting lists in the front
- Fix infinite loading on basket details when adding a recipe that contains an entry in common with another added recipe

## v2.14.0
- Enable withCredentials: true
- Fix pipe Readable number for float between 0 and 1
- Fix countless units display
- Qté => Quantité

## v2.13.0
- Add new user identification method
- Fix pft products display

## v2.12.1
- Fix helper is now pushed in the DOM like details modal
- Fix recipe details modal not showing from basket preview line
- Fix recipe details modal not closing when invalid pos callback triggered

## v2.12.0
- Fix context service basket.stats$ behaviorsubject transformed to simple observable
- Add helper modal on recipe card
- Fix recipe price not updating when price = 0
- Adapt readable float number filter unit to plural / singular
- Avoid showing item replacement button if no candidate available
- Add supplier.load to contextService (and use this id to fetch suggestions)
- Allow client to define callback function if pos invalid
- Remove recipe active/reviewed attributes

## v2.11.0
- Add method to mass assign recipe shares
- Fix null basketStats on ContextService
- Avoid showing recipe card when null

## v2.10.1
- Fix missing import on basket entries service

## v2.10.0
- Add mediaMatcher from material CDN
- Load Pos from external id
- fix basket life cycle

## v2.9.0
- Fix styling
- Refactor recipe details modal & switch between details & basket preview
- Add confirm delete button
- Add Recipes history component

## v2.8.0
- Update styling on most components (recipe-card, recipe-details, counter-input, basket-preview, replace-item, modal...)
- Add guests mecanism (updating guests on recipe card reflects on recipe details and basket lines)
- Add transition between recipe details and basket preview
- Abstract suggestion mecanism from recipe card
- Icons can now be customized on lib initialization
- New classification for CSS classes throughout the lib, and update documentation accordingly

## v2.7.1
- Fix backward incompatibility

## v2.7.0
- Expose basket entries to context service
- Fix circular dependency on baskets
- Fix abstract component calling - ingredient_str on recipes without ingredients
- Fix propagation open recipe when using counter
- Fix recipe form

## v2.6.0
- Fix webcomponent in an angular application build
- Add angular-calendar dependency
- New feature calendar (component ,resource, service)
- New feature favorit list (component ,resource, service)
- Add an abstact level share between recipe-card and display card
- Fix circular dependency between groceries list service and context service

## v2.5.0
- Upgrade angular to 10
- Retrieve recipe partner from user info & set in context service
- Add recipe likes service

## v2.4.3
- Fix model index export list

## v2.4.2
- Fix web components  services  injections

## v2.4.1 
- fix ressource and service export and build

## v2.4.0
- Add recipe form component
- Add new API services

## v2.3.2
- Fix analytics tracking

## v2.3.1
- Fix analytics tracking by specifying a camp## v1.0.1

## v2.2.1
- Fix app initialization (remove initializer for recipe status / recipe provider)
- Adjust counter input styling (remove em / rem)
- Fix analytics events not including referrer

## v2.2.0
- Add token interceptor
- Add recipe suggestions

## v2.1.0
- Update documentation
- New view recipe detail
- Add CORS warning message

## v2.0.3
- Fix analytics

## v2.0.2
- Add analytics
- Append recipes
- CSS adjustments

## v2.0.1
- Flag moment as lib dependency

## v2.0.0
- Add webcomponents lib build & following components :
  - Basket preview
  - Counter
  - Drawer (with pos selection & basket preview)
  - Loader
  - Order button
  - Pos selection
  - Pos card
  - Recipe card
- Add services to interact with Miam API
- Improve Angular demo & add Webcomponents demo

## v1.2.1
- Use environment for paths
- Update package

## v1.2.0
- Fix build
- Update readme
- Remove active and suggested forbidden attributes
- Open basket on save
- Use ngx-jsonapi 2.1.12
- Externalize init factory

## v1.1.0
- Lock node version on gitlab
- Adjust button styling and use sessionStorage for list id
- Fix types in order component
- Fix list return & button animations
- Fix recipe check before creation
- Fix static @dynamic class anotation
- Add groceries list
- Fix removed recipe source in types
