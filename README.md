# NgMiamSdk

NgMiam SDK is an open-source library letting you enhance your customer experience using **[Miam](https://miam.tech)**'s technology.

In order to be able to let you use its capabilities regardless of your Javascript framework, we've designed it as a WebComponents library that you can inject anywhere.

Two builds are available :

- **[webc-miam](https://www.npmjs.com/package/webc-miam)** (built as WebComponents)
- **[ng-miam](https://www.npmjs.com/package/ng-miam)** (built as an Angular lib)

This project was generated with Angular CLI version 8.2.14.

**Table of contents**

[[_TOC_]]
 

## Getting started

> :warning: You will need to be granted access to api.miam.tech to make the most of this SDK. Please contact us at contact@miam.tech. You may experience **CORS** issues otherwise.

### CDN Installation (framework agnostic):

An easy way to install webc-miam is by using the CDN. 

* Add the following tag to your page.

```html
 <script type="module" src="https://cdn.jsdelivr.net/npm/webc-miam@2.0.2/webc-miam.js"></script>
```

* Then, start configuring the library

For instance, you will have to set a point of sale if you want to be able to create a basket based on recipes. 

> The whole list of pos is available on our API at the following end point :
> https://api.miam.tech/api/v1/point-of-sales

```html
 <script>
  // POS should be loaded on application start.
  // Changing the pos id after the application is started should recalculate the basket
    window.miam.pos.load('1');
  </script>
 ```

> Not that you can set it later with `webc-pos-selection`

### Local installation (for Angular project):

```
npm install ng-miam
```

* Import NgMiamModule to your AppModule:

```typescript
import { MIAM_PROVIDER_ID, MIAM_STATUS_ID, NgMiamModule } from "ng-miam";

@NgModule({
  imports: [
    NgMiamModule
  ],
  // Deprecated
  providers: [
    { provide: MIAM_STATUS_ID, useValue: '2' },
    { provide: MIAM_PROVIDER_ID, useValue: '5' }
  ]
})
```

* Then provide a point of sale (pos) in you app.component.ts

> The whole list of pos is available on our API at the folowing endpoint :
> https://api.miam.tech/api/v1/point-of-sales

```typescript
import { ContextService } from 'ng-miam';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(
    private contextService: ContextService
  ) {
    contextService.miam.pos.load('103');
    // Other way to do it : (window as any).miam.pos.load('104');
  }
}
```

> If you want to inject WebComponents instead of Angular components in an Angular app, just import the corresponding js file in you index.html
> then you will have to add CUSTOM_ELEMENTS_SCHEMA into your app.module.ts.


```typescript
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'

import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
  bootstrap: [AppComponent]
})
```

## Features

  **You're all set!** now, let's get a closer look at our libray.

  WebComponents have inputs and outputs.

  You can pass input parameters to a WebComponent simply by adding properties to the corresponding HTML tag :

  ```html
  <!--  index.html-->
  <script>src="miam-recipe-card.js"</script>
  <miam-recipe-card addToCartText="textYouWantToShow" ></miam-recipe-card>
  ```

  If you need to subscribe to the Webcomponents's outputs (events), you will have to add an EventListener to the corresponding DOM element :

  ```javascript
  const component = document.querySelector('miam-recipe-card');
  component.addEventListener('display', (event) => {
    alert(event);
  }
  ```

### Order with Miam button

![alt text](src/assets/orderButton.png "order button")

> (*) = mandatory

* Inputs :  

  - source (*) : string
  - recipe : MiamRecipe 
  - text : string
  - openOnSave : boolean 


* Outputs :

  - saved : GroceriesList
  - error : HttpErrorResponse


#### With Angular :

Use the `MiamRecipe` type to define your recipe. The button will then request Miam API to persist the Recipe and add it to your current groceries list. 

```typescript
import { MiamRecipe } from "ng-miam";
@Component({})
export class AppComponent {
  public myRecipe: MiamRecipe = {
    'title': 'Smoothie à la banane',
    'media-url': 'https://image-url.miam.tech',
    'preparation-time': "PT5M",
    'cooking-time': "PT5M",
    'ingredients': ['1l de lait', '100g de glace', '70g de banane']
  };
}
```


```html
<ng-miam-order-button
  [recipe]="myRecipe"
  (onSaved)="redirectToList($event.id)"
  text="Order with Miam">
</ng-miam-order-button>
```

If you don't define any Recipe yourself, the button will send the current location (URL) to the API and attempt to scrap & parse its content in order to build the Recipe and add it to your current groceries list.

```html
<ng-miam-order-button
  [source]="marmiton.org"
  (onSaved)="redirectToList($event.id)"
  text="Order with Miam">
</ng-miam-order-button>
```

#### With WebComponent:

With a Recipe defined:

```html
<webc-miam-order-button 
  source ="https://www.test.fr/recettes/12.aspx" text="I want to order with Miam">
</webc-miam-order-button >
<script>
  let miamrecipe = {
    'title': 'Smoothie à la banane',
    'media-url': 'https://image-url.miam.tech',
    'preparation-time': "PT5M",
    'cooking-time': "PT5M",
    'ingredients': ['1l de lait', '100g de glace', '70g de banane']
  };
   const component = document.querySelector('webc-miam-order-button');
   component.recipe = miamrecipe;
</script>
```

Without defining any Recipe:

```html
<webc-miam-order-button 
  text="Order with Miam"
  source = "marmiton.org" >
</webc-miam-order-button >
```

### Point of sale selector

![alt text](src/assets/pos.png "Point of sale")

* Input :

  - blockTitle : string

> No output on this component

#### With Angular :

```html
// note that blockTitle is not mandatory default value will be : 'Mon magasin'
 <ng-miam-pos-selection blockTitle="Select your favorite store !"></ng-miam-pos-selection>
```

#### With WebComponent :

```html
 <webc-miam-pos-selection blockTitle='Select your favorite store !'></webc-miam-pos-selection>
```

### Basket preview

![alt text](src/assets/basketPreview.png "basket preview")


* Inputs :
 
   - blockTitle : string
   - recipeId : string (defining a recipeId will display only a single recipe in the preview) 
   - showNotice : boolean (notify the user on ho he can add more recipes to his basket)

 > None of thoses inputs are mandatory.

 * Outputs :
  
   - statsUpdated : BasketStats

#### With Angular :

```html
 <ng-miam-basket-preview-block [blockTitle]="'Miam rocks !'" (statsUpdated)="yourMethod($event)"></ng-miam-basket-preview-block>
```

Single recipe mode :

```html
 <ng-miam-basket-preview-block [recipeId]='recipeId' [blockTitle]="'Miam still rocks !'" (statsUpdated)="yourMethod($event)"></ng-miam-basket-preview-block>
```

#### With WebComponent :

```html
 <webc-miam-basket-preview-block blockTitle='Miam rocks again !'></webc-miam-basket-preview-block>

  <script>
  const component = document.querySelector('webc-basket-preview-block');
  component.addEventListener('statsUpdated', (event) => {
    alert(event);
  });
  </script>
```

Single recipe preview :


```html
 <webc-miam-basket-preview-block  recipeId='recipeId' blockTitle='Miam rocks again !'></webc-miam-basket-preview-block>

  <script>
  const component = document.querySelector('webc-basket-preview-block');
  component.addEventListener('statsUpdated', (event) => {
    alert(event);
    });
  </script>
```

### Drawer

![alt text](src/assets/drawer.png "Miam drawer")

 > Note that our drawer z-index is of 6000 on the overlay and 6001 on the drawer it self 

 This component already includes points of sale selection and basket preview components

 #### With Angular :

```html
 <ng-miam-drawer></ng-miam-drawer>
```

#### With WebComponent :

```html
 <webc-miam-drawer></webc-miam-drawer>
```


### Recipe card

![alt text](src/assets/recipeCard.png "Recipe card")

> (*) = mandatory

* Inputs :

   - addToCartText :string (default text : 'Ajouter la recette au panier')
   - moreInfo: string  (default text : 'Voir la recette')
   - context (*) : { shelfIngredients: Array<string>,
                currentIngredients: Array<string>,
                basketIngredients: Array<string> }

 #### With Angular :

```html
 <ng-miam-recipe-card [addToCartText]="'Add to cart'" [moreInfo]="'get details'" [context]="yourContext"></ng-miam-recipe-card>
```

#### With WebComponent :

```html
 <webc-miam-recipe-card addToCartText='Add to cart' moreInfo='get details'></webc-miam-recipe-card>
  <script>
    const card = document.querySelector('webc-miam-recipe-card');
    const context = {
      shelfIngredients: [id1,id2,id3], currentIngredients: [id4,id5], basketIngredients: []
    }
    card.context = context;
    // our web component detect only referencies changes  
    // if you want to update card.context just pass a new reference
    // like : card.context = {... card.context}
  </script>
```
> You need PoS id setted for this feature

### Miam's modal

![alt text](src/assets/modal.png "no header modal")

> this popup z-index is 6001 and 6000 for the overlay

* Inputs :

    - title: string (if your in header mode)
    - expanded: boolean (use to show and hide modal)
    - confirmButtonText: string (if empty will not show any button)
    - cancelButtonText: string (if empty will not show any button)
    - isAngularComponent: boolean (angular use ng content than webcomponent use slots)
    - noHeaderMode: boolean (if true remove the header)

* Outputs :

    - close: void
    - cancel: void
    - confirm: void

>>>
  Those outputs are justs signals with no return value.

  From this modal you should use a service.
>>> 

#### With Angular :

With header

```html
 <ng-miam-modal
  [isAngularComponent]="true"
  title="Les articles suivants on été ajoutés à votre panier :"
  cancelButtonText="Retirer les articles du panier"
  confirmButtonText="Confirmer la sélection"
  [expanded]="expanded"
  (close)="close()"
  (cancel)="remove()">
  <!-- this part will be injected into the popup -->
  <section>
    <ng-miam-basket-preview-block [recipeId]="recipe?.id"></ng-miam-basket-preview-block>
  </section>
</ng-miam-modal>
```

without header

```html
<ng-miam-modal 
  [isAngularComponent]="true" 
  [noHeaderMode]='true' 
  [title]="recipe?.title" 
  [expanded]='expanded'
  (close)='close()'>
 <ng-miam-recipe-details [addToCartText]="addToCartText" [recipe]="recipe"></ng-miam-recipe-details>
</ng-miam-modal>
```

> Note that you can nest multiple Angular components to get one final WebComponent. 

#### With WebComponent :

```html
 <webc-miam-modal
  title="Les articles suivants on été ajoutés à votre panier :"
  cancelButtonText="Retirer les articles du panier"
  confirmButtonText="Confirmer la sélection"
  >
  <!-- this part will be injected into the popup -->
  <h1>Hello Miam !</h1>
</webc-miam-modal>
<button id="my-button">Open modal</button>
<script>
  const component = document.querySelector('webc-miam-modal');
  const button = document.getElementById('my-button');

  component.addEventListener('close', (event) => {
    alert('closed');
    });
    
  component.addEventListener('cancel', (event) => {
    alert('canceled');
    });

  button.addEventListener('click', (event) => {
      component.expended=true ;
    })
</script>
```

### How-to implement your own WebComponent

If you want to create your own Miam WebComponents, be advised that this lib does not use zone.js, but onPushStrategy instead.

More info **[here](https://netbasal.com/a-comprehensive-guide-to-angular-onpush-change-detection-strategy-5bac493074a4)**.

OnPushStrategy avoids common pitfalls of Angular elements. Add it to your component's decorator:

```typescript
import { Component, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ng-miam-your-component',
  templateUrl: './your-component.component.html',
  styleUrls: ['./your-component.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})

```

WebComponents can use ShadowDom.

More info **[here](https://angular.io/api/core/ViewEncapsulation)**. 

Most of them use viewEncapsulation "None" to enable a more flexible CSS customization within the host Web application.

> :warning: with viewEncapsulation none, (if not properly scoped), your CSS may bleed & override the design of your host Web application.

Add your WebComponent to the folder  `/projets/ng-miam/src/_webcomponents`

And declare it within the corresponding module: `web-components.module.ts`

```typescript
import { NewWebComponent }  from './newWebComponent/newWebComponent.component';

export const WEB_COMPONENTS = [ NewWebComponent ];
export const WEB_COMPONENTS_NAMES = [ 'new-web-component'] ; // no uppercase allowed here
```

Last step: build and pack the library :

```
npm run build:pack:webc
```

The lib will be bundled to the directory: `dist/webc-miam`. All the useful Javascript files are concatenated in `webc-miam.js`.

Your custom component will be prefix by webc-miam :

```html
<webc-miam-your-component></webc-miam-your-component>

```

## Running the demo application

To help you getting started, a demo application is available under `/src/app`. You can run it locally like any angular app.

### Development server

#### Run the Angular app demo

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

#### Run the WebComponents app demo

We also provide a demo showcasing the WebComponents library

Run `npm run build:pack:webc` to build the lib

then 

Run `npm run start:webc` 

demo is available on **[localhost](http://localhost:4200/)** port 4200

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

#### Build the Angular library

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

#### Build the WebComponents library

```
npm run build:pack:webc
```

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


## Styling

You can easily use a custom theme simply by overriding our css variables

```html
<style>
  :host{
    --miam-color-primary:blue;
    --miam-color-black: #000001;
    --miam-default-transition: all 1s linear;
    --miam-border-radius : 3px
  }
</style>
```
> note that there also local variables at component's level 
 
### Color theme

 | CSS variable |  Default value |
 |:-------------|:-------------:|
 | --miam-color-primary | `#2bb6aa` |
 | --miam-color-primary-light  | `#eafaf9` |
 | --miam-color-primary-dark  | `#419f97` |
 | --miam-color-primary-text  | --miam-color-white |
 | --miam-color-secondary  | `#ef760f` |
 | --miam-color-secondary-light | `#FAEBD7` |
 | --miam-color-secondary-dark  | `#ef7711` |
 | --miam-color-secondary-text  | --miam-color-white|
 | --miam-color-ternary  | `#1accf8` |
 | --miam-color-ternary-light  | `#cef4fd` |
 | --miam-color-ternary-light  | `#cef4fd` |
 | --miam-color-ternary-dark  |  `#057894` |
 | --miam-color-ternary-text  | --miam-color-white |
 | --miam-color-success  |  --miam-color-primary |
 | --miam-color-success-text  | --miam-color-white |
 | --miam-color-info | --miam-color-primary |
 | --miam-color-info-text | --miam-color-white |
 | --miam-color-warning | --miam-color-secondary |
 | --miam-color-warning-text | --miam-color-white |
 | --miam-color-danger | --miam-color-secondary-dark |
 | --miam-color-danger-text | --miam-color-white |
 | --miam-color-grey | `#f5f5f5` |
 | --miam-color-grey-text | `#acb0b9` |
 | --miam-color-grey-text-dark | `#757575` |
 | --miam-color-white | `#ffffff` |
 | --miam-color-black | `#000000` |

### Font

| CSS variable |  Default value |
|:-------------|:-------------:|
|--miam-font-size-small | `14px`|
|--miam-font-size-medium | `16px`|
|--miam-font-size-large | `20px`|
|--miam-font-size-Xlarge| `24px`|
|--miam-font-weight-light|`300`|
|--miam-font-weight-normal|`400`|
|--miam-font-weight-semibold|`500`|
|--miam-font-weight-bold|`700`|

### Border

| CSS variable |  Default value |
|:-------------|:-------------:|
| --miam-border-radius | `6px` |


### Tansition

| CSS variable |  Default value |
|:-------------|:-------------:|
| --miam-default-transition | `all 0.3s ease-in-out`|

### Elevation

| CSS variable |  Default value |
|:-------------|:-------------:|
|--miam-shadow-small|`-2px 2px 5px 0 rgba(67, 86, 100, 0.12), -1px 1px 2px 0 rgba(67, 70, 74, 0))`|
### Z-index


| CSS variable |  Default value |
|:-------------|:-------------:|
|--miam-z-index-position-absolute-low | `0` |
|--miam-z-index-position-absolute-height | `1`|
|--miam-z-index-loader | `2`|
|--miam-z-index-modal-overlay|`6000`|
|--miam-z-index-modal|`6001`|
|--miam-z-index-drawer-overlay|`5000001`|
|--miam-z-index-drawer-container|`5000002`|

### Custom icons

You can override globally miam's icons.
you'll have to use  window's miam item and call 'overrideIcon' function.

> OverrideIcon take an icons name and an url as parameter

Icons name have to figure in the following list :

| Icon name |  Default  |
|:-------------|:-------------:|
| ArrowDown | ![alt text](src/assets/arrowdown.svg "ArrowDown") |
| Basket | ![alt text](src/assets/basket.svg "Basket") |
| Calendar | ![alt text](src/assets/calendar.svg  "Calendar") |
| Cart | ![alt text](src/assets/cart.svg  "cart") |
| CheckCircleFill | ![alt text](src/assets/checkcirclefill.svg "CheckCircleFill") |
| CheckList | ![alt text](src/assets/checklist.svg  "CheckList") |
| ChevronDown | ![alt text](src/assets/cheverondown.svg  "CheckList") |
| Clear | ![alt text](src/assets/clear.svg  "Clear") |
| CloudUpload | ![alt text](src/assets/cloudupload.svg  "CheckList") |
| CostHight | ![alt text](src/assets/cost/eleve.svg  "CostHight") |
| CostMedium | ![alt text](src/assets/cost/moyen.svg  "CostMedium") |
| CostLow | ![alt text](src/assets/cost/faible.svg  "CostLow") |
| DifficultyHight | ![alt text](src/assets/difficulty/difficile.svg  "DifficultyHight") |
| DifficultyMedium | ![alt text](src/assets/difficulty/moyen.svg  "DifficultyMedium") |
| DifficultyLow | ![alt text](src/assets/difficulty/facile.svg  "DifficultyLow") |
| Euro | ![alt text](src/assets/euro.svg  "Euro")|
| Grip | ![alt text](src/assets/grip.svg  "grip") |
| Grocery | ![alt text](src/assets/grocery.svg  "Grocery") |
| Heart | ![alt text](src/assets/heart.svg  "Heart") |
| Hot | ![alt text](src/assets/hot.svg  "Hot")|
| MinusSquare | ![alt text](src/assets/minussquare.svg  "CheckList") |
| People | ![alt text](src/assets/people.svg  "People")|
| PlusSquare | ![alt text](src/assets/plussquare.svg  "PlusSquare") |
| Picture | ![alt text](src/assets/picture.svg  "Picture") |
| ReturnLeft | ![alt text](src/assets/returnleft.svg  "ReturnLeft") |
| Search | ![alt text](src/assets/search.svg  "Search") |
| Share | ![alt text](src/assets/share.svg  "Share") |
| Swap | ![alt text](src/assets/swap.svg  "Swap") |
| Time | ![alt text](src/assets/time.svg  "Time") |
| Trash | ![alt text](src/assets/trash.svg  "Trash") |
| Wait | ![alt text](src/assets/wait.svg  "Wait") |

#### Custom icons for Ng-miam 

```typescript
import { ContextService } from 'ng-miam';

export class AppComponent implements OnInit {

  constructor(private contextService: ContextService) {}

  ngOnInit(): void {    
    this.contextService.miam.overrideIcon( "Heart",  "./assets/heart.svg")
  }
```

#### Custom icons for Webc-miam

```html
    <script>
      window.miam.overrideIcon( "Heart",  "./heart.svg")
    </script>
```
You can easily use a custom theme simply by overriding our css variables

## Component Styling

You can override style classes of our components like below.

```html
<style>
  :host{
     .miam-recipe-card{
       border-color : solid blue 1px;
     }
  }
</style>
```
Here is the list of style classes for each components 

### Basket preview - block

![alt text](src/assets/basketPreviewBlockClasses.png "basket preview block classes")

### Basket preview - line

![alt text](src/assets/basketPreviewLineClasses1.png "basket preview line classes")

### Basket preview - line (left)

![alt text](src/assets/basketPreviewLineClasses2.png "basket preview line classes")

### Basket preview - line (right)

![alt text](src/assets/basketPreviewLineClasses3.png "basket preview line classes")

### Basket preview - disabled

![alt text](src/assets/basketPreviewDisabledClasses.png "basket preview disabled classes")

### Counter Input

![alt text](src/assets/counterInputClasses.png "counter input classes")

### Miam modal 

![alt text](src/assets/modalClasses.png "cssclasses on miam modale")

> Note that a large part of this modal style depend on injected content. 

### Recipe Card

![alt text](src/assets/recipeCardClasses.png "replace item classes")

### Replace item

![alt text](src/assets/replaceItemClasses.png "replace item classes")


