console.log('--- Init Miam ---')

window.miam.analytics.init('test-ga-key');
window.miam.user.loadWithExtId('test-dev-1', true)

// POS should be loaded on application start.
// Changing the pos id after the application is started should recalculate the basket
window.miam.pos.load('104');
window.miam.supplier.load(3);

// Comment if you test recipe-likes, uncomment to test icon replacement
// window.miam.overrideIcon( "Heart",  "./heart.svg") 

// Set a auth0 token here to test components that need you to be connected, such as favorite list
// window.miam.user.setToken('')

// Display basket stats in div
const basketStats = document.getElementById('basketStats');
window.miam.basket.stats$.subscribe(stats => {
  if (stats) {
    const statsStr = stats.recipesCount + ' repas, ' + stats.entriesCount + ' articles, pour un total de ' + stats.totalPrice.toFixed(2) + ' €';
    setTimeout(() => basketStats.innerHTML = statsStr, 250);
  }
});

// Handle recipe details display/hide
function prepareRecipeDisplay() {
  // When recipe to display appears in URL, trigger the modal
    if (window.location.search.includes('displayRecipe=')) {
      const matcher = window.location.search.match(/displayRecipe=(\d+)/)
      const recipeId = matcher && matcher[1];
      if (recipeId) {
        window.miam.recipes.display(recipeId);
      }
    }
  
  // When the modal is hidden, redirect to home
  window.miam.recipes.hidden.subscribe(function(_event) {
    if (window.location.search.match(/displayRecipe=(\d+)/)) {
      window.location.href = '/';
    }
  })
}
prepareRecipeDisplay();