// Change to true to enable dev mode
const dev = false;

export const environment = {
  development: dev,
  miamAPI: dev ? 'http://localhost:3000' : 'https://api.miam.tech',
  miamWeb: dev ? 'http://localhost:4200' : 'https://miam.tech'
};
