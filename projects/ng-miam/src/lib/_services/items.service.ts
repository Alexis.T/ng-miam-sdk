import { Injectable } from '@angular/core';
import { Service } from 'ngx-jsonapi';
import { Item } from '../_models/item';

@Injectable({
  providedIn: 'root'
})
export class ItemsService extends Service<Item> {
  public resource = Item;
  public type = 'items';

  constructor() {
    super();
    this.register();
  }
}
