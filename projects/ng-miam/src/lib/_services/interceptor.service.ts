import { Injectable, Injector } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpErrorResponse,
  HttpEvent,
  HttpResponse,
  HttpParams
} from '@angular/common/http';

import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { UserService } from './user.service';
import { ContextService } from './context.service';


@Injectable()
export class MiamInterceptor implements HttpInterceptor {


  constructor(private injector: Injector) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): any {

    const token = localStorage.getItem('_miam/userToken');
    const userId = localStorage.getItem('_miam/userId');
    if (token) {
      request = request.clone({
        setHeaders: { Authorization: `Bearer ${token}` }
      });
    } else if (userId) {
      request = request.clone({
        setHeaders: { Authorization: `user_id ${userId}` }
      });
    }

    request = request.clone({ withCredentials: true });

    const usersService = this.injector.get(UserService);
    if (usersService?.userInfo?.forbidProfiling) {
      request = request.clone({ params: new HttpParams().set('profiling', 'off') });
    }

    return next.handle(request).pipe(map((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        if (event?.body?.links) {
          const links = event.body.links;
          const lastPageParams = new URLSearchParams(new URL(links.last).search);
          const size = parseInt(lastPageParams.get('page[size]'), 10);
          const total = size * parseInt(lastPageParams.get('page[number]'), 10);
          let current = parseInt(lastPageParams.get('page[number]'), 10);

          if (links.next) {
            const nextPageParams = new URLSearchParams(new URL(links.next).search);
            current = parseInt(nextPageParams.get('page[number]'), 10) - 1;
          }

          // ngx-jsonapi will put this info in DocumentCollection<any>.page
          Object.assign(event.body, {
            meta: {
              size,
              resources_per_page: size,
              number: current,
              total_resources: total
            }
          });
        }
      }

      return event;
    }), catchError((error: HttpErrorResponse) => {
      // status 0 = unknown error, CORS break on preflight then no relevent error message
      // Warning : circular dependency injection?
      const context = this.injector.get(ContextService);
      if (error.status === 0 && error.url.includes('https://api.miam.tech/api') && context) {
        context.setCORSIssueState(true);
      }
      return throwError(error);
    }));
  }
}
