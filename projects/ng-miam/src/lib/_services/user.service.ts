import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { RecipeProviderService } from "../_services/recipe-provider.service";
import { BehaviorSubject, Observable, of } from "rxjs";
import { take, switchMap, skipWhile, tap } from "rxjs/operators";
import { environment } from "../environment";
import { RecipeProvider } from "../_models";
import { User } from "../_models/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userProviders$: BehaviorSubject<RecipeProvider>;
  private userRoles$: BehaviorSubject<string[]>;
  public isLogged$ = new BehaviorSubject(false);
  public userInfo: User;

  constructor(private http: HttpClient, private recipesProvidersService: RecipeProviderService) {
    this.userRoles$ = new BehaviorSubject([]);
    this.userProviders$ = new BehaviorSubject(undefined);
  }

  public get userProvider(): Observable<RecipeProvider> {
    return this.userProviders$.asObservable().pipe(skipWhile(provider => provider === undefined));
  }

  public get userRoles(): Observable<string[]> {
    return this.userRoles$.asObservable();
  }

  public get isAdmin(): Observable<boolean> {
    return this.userRoles$.pipe(switchMap((roles: string[]) => of(roles.includes('Admin'))));
  }

  public setUserProviderManually(provider: RecipeProvider) {
    this.userProviders$.next(provider);
  }

  public toggleIsLogged(logged: boolean) {
    this.isLogged$.next(logged);
  }

  public setUserInfo(forbidProfiling): Observable<any> {
    this.userInfo = new User({ forbidProfiling: forbidProfiling });
    return this.http.get(`${environment.miamAPI}/user_info`).pipe(take(1), switchMap(userInfo => {
      Object.assign(this.userInfo, userInfo);
      this.initRoles();
      return this.initProvider();
    }));
  }

  public resetUserInfo() {
    this.userInfo = null;
    this.userRoles$.next([]);
    this.userProviders$.next(null);
  }

  private initProvider(): Observable<any> {
    if (this.userInfo?.app_metadata?.recipe_provider) {
      return this.recipesProvidersService.get(this.userInfo.app_metadata.recipe_provider).pipe(skipWhile(res => res == null),
        tap(provider => this.userProviders$.next(provider)));
    } else {
      return this.recipesProvidersService.get('personal').pipe(skipWhile(res => res == null),
      tap(provider => this.userProviders$.next(provider)));
    }
  }

  private initRoles(): void {
    this.userRoles$.next(this?.userInfo?.app_metadata?.roles ? this.userInfo.app_metadata.roles : []);
  }

}