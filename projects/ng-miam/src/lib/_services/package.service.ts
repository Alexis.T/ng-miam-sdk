import {Injectable} from '@angular/core';
import {DocumentCollection, Service} from 'ngx-jsonapi';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Package } from '../_models/package';
import { RecipeProviderService } from './recipe-provider.service';
import { RecipeStatusService } from './recipe-status.service';

@Injectable({
  providedIn: 'root'
})
export class PackageService extends Service<Package> {
  public resource = Package;
  public type = 'packages';

  constructor(
    private providerService: RecipeProviderService,
    private statusService: RecipeStatusService
  ) {
    super();
    this.register();
  }

  categories_for(suppliersIds): Observable<DocumentCollection<Package>> {
    return this.statusService.active.pipe(switchMap((activeStatus) => {
      return this.all({ remotefilter: { category_for: suppliersIds.join(','), status: activeStatus.id} });
    }))
  }
}