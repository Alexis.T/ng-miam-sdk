import {Injectable} from '@angular/core';
import {Service} from 'ngx-jsonapi';
import {skipWhile, switchMap, tap, take} from 'rxjs/operators';
import {RecipeProvider} from "../_models/recipe-provider";
import {BehaviorSubject, Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RecipeProviderService extends Service<RecipeProvider> {

  resource = RecipeProvider;
  type = 'recipe-providers';
  private provider: RecipeProvider;
  private providerList$ = new BehaviorSubject<RecipeProvider[]>(null);

  constructor(){
    super();
    this.register();
    this.all().pipe(
      skipWhile(res => res.is_loading),
      take(1)).subscribe(res => {
        this.providerList$.next(res.data);
      });
  }

  get providerList(): Observable<RecipeProvider[]> {
    return this.providerList$.asObservable().pipe(skipWhile(res => res == null));
  }

  init(id: string) {
    this.register();
    return this.loadCachedProvider(id).toPromise();
  }

  getCached() {
    return this.provider;
  }

  loadCachedProvider(id: string) {
    return of(id).pipe(
      switchMap(_ => this.get(id)),
      tap(status => this.provider = status)
    );
  }

}
