import { Injectable } from '@angular/core';
import { DocumentResource, Service } from 'ngx-jsonapi';
import { RecipeStep } from '../_models/recipe-step';
import { Recipe } from '../_models/recipe';
import pullAt from 'lodash-es/pullAt';
import { forkJoin, Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})

export class RecipeStepsService extends Service<RecipeStep> {

  public resource = RecipeStep;
  public type = 'recipe-steps';

  constructor() {
    super();
    this.register();
  }


  /**
   * Merge Recipe steps list with output of recipe forms Step list
   * it's link with RecipeFormComponent
   * @param recipe to update
   * @param stepsForm list of step output of RecipeFormComponent form
   */
  public formToStepsMapper(recipe: Recipe, stepsForm: any): RecipeStep[] {
    const oldRecipeSteps = [...recipe.relationships['recipe-steps'].data];
    const newRecipeSteps = [];

    stepsForm.forEach((step, index: number) => {
      if (step.id) {
        const oldIndex = oldRecipeSteps.findIndex(el => el.id === step.id);
        oldRecipeSteps[oldIndex].attributes = { ...oldRecipeSteps[oldIndex].attributes,
           ['step-number']: index, description: step.attributes.description };
        newRecipeSteps.push(pullAt(oldRecipeSteps, [oldIndex])[0]);
      } else {
        newRecipeSteps.push(this.newStep(step, recipe, index));
      }
    });
    return newRecipeSteps;
  }

  private newStep(step, recipe: Recipe, index: number): RecipeStep {
    const newStep = this.new();
    newStep.addRelationship(recipe, 'recipe');
    newStep.attributes = { ...newStep.attributes, ['step-number']: index, description: step.attributes.description };
    delete newStep.id;
    return newStep;
  }

  public createOrUpdateRelatedSteps(recipeSteps: RecipeStep[]): Observable<DocumentResource<RecipeStep>[]> {
    return forkJoin(recipeSteps.map((step) => {
      return step.save() as Observable<DocumentResource<RecipeStep>>;
    }));
  }

  public deleteStepList(stepIds: string[]): Observable<any> {
    return forkJoin(stepIds.map(stepId => {
      return this.delete(stepId);
    }));

  }
}
