
import { Injectable } from '@angular/core';
import { Service } from 'ngx-jsonapi';
import { CalendarEvent } from '../_models/calendar-event';

@Injectable({
  providedIn: 'root'
})
export class CalendarEventsService extends Service<CalendarEvent> {

  public resource = CalendarEvent;
  public type = 'calendar-events';

  constructor() {
    super();
    this.register();
  }
}