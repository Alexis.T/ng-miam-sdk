import { EventEmitter, Injectable } from '@angular/core';
import { iif, of, Observable, forkJoin, BehaviorSubject, combineLatest } from 'rxjs';
import { Service, DocumentCollection } from 'ngx-jsonapi';
import { Recipe } from '../_models/recipe';
import { RecipeProviderService } from './recipe-provider.service';
import { RecipeStatusService } from './recipe-status.service';
import { RecipeTypeService } from './recipe-type.service';
import { GroceriesListsService } from './groceries-lists.service';
import { RecipeEventsService } from './recipe-events.service';
import { debounceTime, map, skipWhile, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environment';
import { IngredientsService } from './ingredients.service';
import { RecipeStepsService } from './recipe-steps.service';
import { MiamRecipe } from '../_types/recipe';
import { SuppliersService } from './suppliers.service';
import { RecipePricing } from '../_models/recipe-pricing';
import { SponsorService } from './sponsor.service';
import { PointOfSalesService } from './point-of-sales.service';

const MIAM_API_HOST = `${environment.miamAPI}/api/v1/`;

interface RecipeDisplay {
  previewAllowed: boolean;
  recipe: Recipe;
  previewMode: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class RecipesService extends Service<Recipe> {

  resource = Recipe;
  type = 'recipes';
  displayedRecipe$ = new BehaviorSubject<RecipeDisplay>(null);
  displayHelper$ = new BehaviorSubject<boolean>(false);
  displayedRecipeChanged = new EventEmitter<void>();
  hidden = new EventEmitter<boolean>();

  private randomSeed: number;
  private pageNumber = 1;
  private recipes: Recipe[] = [];

  constructor(
    private http: HttpClient,
    private providerService: RecipeProviderService,
    private statusService: RecipeStatusService,
    private typeService: RecipeTypeService,
    private suppliersService: SuppliersService,
    private posService: PointOfSalesService,
    private listsService: GroceriesListsService,
    private ingredientsService: IngredientsService,
    private recipeStepsService: RecipeStepsService,
    private recipeEventsService: RecipeEventsService,
    private sponsorService: SponsorService
  ) {
    super();
    this.register();
    this.intRandomSeed();
  }

  loadRecipes(): Observable<Recipe[]> {
    const list = this.listsService.list$.value;
    if (!list) {
      return of(null);
    } else if (list.attributes['recipes-infos'].length === 0) {
      return of([]);
    }

    return forkJoin(list.attributes['recipes-infos'].map(info => {
      return this.getOrFetch(info.id).pipe(
        skipWhile(r => r.is_loading),
        tap(r => r.modifiedGuests = info.guests)
      );
    }));
  }

  // Create recipe & append to groceries list
  getOrCreate(recipe, openBasket: boolean = false) {
    const provider = this.providerService.getCached();
    const extId = this.createExtId(recipe.title);

    // Check if exists or create it
    return this.getRecipe(provider.id, extId).pipe(
      skipWhile(r => r.is_loading),
      // NOTE : adding of(void 0) for falsy observable (ngx-jsonapi is triggering an http request when save() called)
      switchMap((r) => iif(() => !!r.data.length, of(r.data[0]), of(void 0).pipe(switchMap(_ => this.create(extId, recipe))))),
      // Append recipe to current saved list
      switchMap((r) => this.listsService.appendRecipeToList(r.id, r.modifiedGuests, openBasket))
    );

  }

  getRandom(page = { number: this.pageNumber, size: 1 }, filters = {}, clear = false): Observable<Recipe[]> {
    this.pageNumber++;
    // Fetch random recipes
    return this.all({
      page,
      remotefilter: Object.assign(filters, { random: this.randomSeed, reviewed: true, exclude_no_picture: true }),
      include: ['ingredients', 'recipe-steps', 'recipe-status', 'recipe-type', 'sponsors']
    }).pipe(
      skipWhile(result => result.is_loading),
      map(result => result.data)
    );
  }

  // DEPRECATED / use TTL instead
  getOrFetch(recipeId): Observable<Recipe> {
    const recipe = this.recipes.find(r => r.id === recipeId);
    if (recipe) {
      return of(recipe);
    } else {
      return this.get(recipeId, { include: ['ingredients', 'recipe-steps', 'recipe-status', 'recipe-type', 'sponsors' ] }).pipe(
        tap(r => this.recipes.push(r))
      );
    }
  }

  // Debounce to let enough time for the pos to be defined
  getSuggestion(
    shelf_ingredients_ids: string[],
    current_ingredients_ids: string[],
    basket_ingredients_ids: string[],
    group_id: string
  ): Observable<Recipe[]> {
    return combineLatest([this.suppliersService.supplier$, this.posService.pos$]).pipe(
      skipWhile(results => !results[0]),
      debounceTime(100),
      switchMap(results => {
        const body = { shelf_ingredients_ids, current_ingredients_ids, basket_ingredients_ids, group_id };
        let supplierFilter = `supplier_id=${results[0].id}`;
        if (results[1]) {
          supplierFilter = `supplier_id=${results[0].id}&pos_id=${results[1].id}`;
        }
        const url = MIAM_API_HOST + `recipes/suggestions?${supplierFilter}&include=ingredients,recipe-steps,sponsors,recipe-type`;
        return this.http.post(url, body);
      }),
      map((returnedRecipes: DocumentCollection<Recipe>) => {
        const recipes = this.newCollection();
        recipes.fill(returnedRecipes);
        return recipes.data;
      })
    );
  }

  getPricing(recipeId, posId, serves = null): Observable<RecipePricing> {
    if (!recipeId) {
      return of(null);
    }

    let url = MIAM_API_HOST + `recipes/${recipeId}/pricing?point_of_sale_id=${posId}`;
    if (serves) {
      url += `&serves=${serves}`;
    }
    return this.http.get(url).pipe(
      map(result => new RecipePricing(result))
    );
  }

  createFromUrl(source: string, ext_link: string): Observable<Recipe> {
    if (!source || !ext_link) {
      throw new Error('No source or recipe link provided');
    }

    return this.http.post(`${environment.miamAPI}/api/v1/recipes/from_url`, { source, ext_link }).pipe(
      map((result: any) => result.data as Recipe)
    );
  }

  // Open modal showing recipe details
  display(recipeId: string, { previewAllowed = true, guests = null, previewMode = false}) {
    if (!recipeId) {
      return;
    }

    this.get(recipeId, { include: ['ingredients', 'recipe-steps', 'recipe-type', 'sponsors'] })
      .pipe(skipWhile(recipe => recipe.is_loading))
      .subscribe(recipe => {
        this.displayObject(recipe, {previewAllowed, guests, previewMode });
      });
  }

  displayObject(recipe: Recipe, { previewAllowed = true, guests = null, previewMode = false}){
    if (!recipe){
      return
    }

    recipe.modifiedGuests = guests || +recipe.guests;
    this.displayedRecipe$.next({ previewAllowed , recipe, previewMode });
    if (!previewMode) {
      this.recipeEventsService.sendEvent(recipe, this.recipeEventsService.ACTION_DISPLAYED);
    }
  }

  toggleHelper() {
    this.displayHelper$.next(!this.displayHelper$.value);
  }

  hide() {
    this.displayedRecipe$.next(null);
    this.hidden.emit(true);
  }

  private create(extId: string, recipe : MiamRecipe) {
    const r = this.new();
    // Copy recipe into attributes
    Object.assign(r.attributes, recipe);
    // Set provider & status (cached from initialized var MIAM_STATUS_ID & MIAM_PROVIDER_ID)
    r.addRelationship(this.providerService.getCached(), 'recipe-provider');
    r.addRelationship(this.statusService.getCached(), 'recipe-status');
    // TODO : hard coded (plat principal as type)
    r.addRelationship(this.typeService.getTypeByName('plat principal'), 'recipe-type');
    // Override specific miam reserved attributes
    r.attributes.source = null;
    r.attributes.description = null;
    r.attributes['ext-id'] = extId;
    return r.save().pipe(
      skipWhile((r: any) => r.is_loading),
      map((r: any) => r.data)
    )
  }

  private createExtId(title: string): string {
    const provider = this.providerService.getCached();
    return `${provider.attributes.name.slice(0, 3).toUpperCase()}-${title.replace(/\s+/gm, '-').toLowerCase()}`;
  }

  private getRecipe(providerId: string, extId: string) {
    return this.all({
      beforepath: `recipe-providers/${providerId}`,
      remotefilter: {
        'ext-id': extId,
        'active': 'true,false'
      }
    })
  }

  /**
   * help us to keep a track of random 
   * and avoid to show same recipe mutiple times
   */
  private intRandomSeed(): void {
    this.randomSeed = Date.now();
  }

}
