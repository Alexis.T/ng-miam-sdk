import { Injectable } from '@angular/core';
import { Service, DocumentCollection, DocumentResource } from 'ngx-jsonapi';
import { Basket } from '../_models/basket';
import { Observable, BehaviorSubject, combineLatest, forkJoin, of } from 'rxjs';
import { skipWhile, switchMap, tap, map, take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment as e } from '../environment';
import { BasketEntriesService } from './basket-entries.service';
import { GroceriesListsService } from './groceries-lists.service';
import { PointOfSalesService } from './point-of-sales.service';
import { BasketPreviewLine, LineEntries } from '../_models/basket-preview-line';
import { RecipesService } from './recipes.service';
import { Recipe } from '../_models/recipe';
import { GroceriesEntriesService } from './groceries-entries.service';
import { BasketEntry } from '../_models/basket-entry';
import { unionBy, flatten } from 'lodash-es';
import { AnalyticsService } from './analytics.service';
import { GroceriesEntry } from '../_models/groceries-entry';

export interface BasketStats {
  totalPrice: number;
  recipesCount: number;
  entriesCount: number;
}

@Injectable({
  providedIn: 'root'
})
export class BasketsService extends Service<Basket> {
  static entriesPerPages = 30;

  constructor(
    private http: HttpClient,
    private basketEntriesService: BasketEntriesService,
    private listsService: GroceriesListsService,
    private groceriesEntriesService: GroceriesEntriesService,
    private posService: PointOfSalesService,
    private recipesService: RecipesService,
    private analyticsService: AnalyticsService
  ) {
    super();
    this.register();
    this.loadBasket().subscribe();
    this.loadPreview();
  }

  resource = Basket;
  type = 'baskets';
  basket$ = new BehaviorSubject<Basket>(null);
  basketPreview$ = new BehaviorSubject<BasketPreviewLine[]>(null);
  basketStats$ = new BehaviorSubject<BasketStats>({ totalPrice: 0, recipesCount: 0, entriesCount: 0 });

  private _entries$ = new BehaviorSubject<BasketEntry[]>([]);

  // Fetch related baskets, filtering on a point of sale
  fetchForListAndPos(listId: string, posId: string): Observable<Basket> {
    if (!listId || !posId) {
      return of(null);
    }
    let url = [e.miamAPI, 'api/v1/groceries-lists', listId, 'baskets'].join('/');
    url = `${url}?filter[point_of_sale_id]=${posId}`;

    return this.http.get(url).pipe(
      skipWhile((resp: DocumentCollection<Basket>) => resp.is_loading),
      map((resp: DocumentCollection<Basket>) => {
        const baskets = this.newCollection();
        baskets.fill(resp);
        return baskets.data[0];
      })
    );
  }

  loadBasket(): Observable<Basket> {
    return combineLatest([this.listsService.list$, this.posService.pos$]).pipe(
      skipWhile(results =>  !results[0] || !results[1]),
      switchMap(results => this.fetchForListAndPos(results[0].id, results[1].id)),
      tap(basket => this.basket$.next(basket))
    );
  }

  // Set basket as confirmed, and returns only once the new current list is returned
  confirmBasket(): Observable<Basket> {
    let confirmedBasket: Basket;
    return this.basket$.pipe(
      skipWhile(b => !b),
      take(1),
      switchMap(basket => {
        basket.attributes.confirmed = true;
        return basket.save();
      }),
      switchMap((resp: DocumentResource<Basket>) => {
        confirmedBasket = this.new();
        confirmedBasket.fill(resp);
        const total = confirmedBasket.totalPrice;
        console.log('[Miam] send push analytics event if > 0:', total, confirmedBasket);
        if (total > 0) {
          // Send total in cents
          this.analyticsService.sendEvent('push-basket', null, (total * 100));
        }
        return this.listsService.resetList();
      }),
      map(() => confirmedBasket)
    );
  }

  basketEntries(): Observable<BasketEntry[]> {
    return this._entries$;
  }

  activeBasketEntries(): Observable<BasketEntry[]> {
    return this.basketEntries().pipe(
      map((entries: BasketEntry[]) => entries.filter(entry => entry.groceriesEntryStatus === 'active'))
    );
  }

  // Decrease quantity on a list of basket entries
  // If the quantity goes below 1, the corresponding groceries entry's status gets deleted
  // Reload basket after all objects are saved
  removeBasketEntries(entriesToRemove: { id: string, qty: number }[]): Observable<any> {
    if (!this._entries$.value) {
      return;
    }

    const saveObservables: Observable<GroceriesEntry|BasketEntry>[] = entriesToRemove.map(entryToRemove => {
      const basketEntry = this._entries$.value.find(be => be.id === entryToRemove.id);
      if (!basketEntry || !entryToRemove.qty) {
        return of(null);
      }

      const finalCount = basketEntry.quantity - entryToRemove.qty;
      if (finalCount >= 1) {
        basketEntry.quantity = finalCount;
        return basketEntry.save();
      } else {
        basketEntry.quantity = 1;
        basketEntry.groceriesEntry.status = 'deleted';
        return forkJoin([basketEntry.save(), basketEntry.groceriesEntry.save()]);
      }
    });

    return forkJoin(saveObservables).pipe(
      switchMap(() => this.listsService.refreshCurrentList()),
      take(1)
    );
  }

  private loadPreview(): Observable<BasketPreviewLine[]> {
    this.basket$.pipe(
      skipWhile(b => !b),
      switchMap(() => this.recipesService.loadRecipes()),
      switchMap(recipes => {
        const basket: Basket = this.basket$.value;
        return this.fetchBasketEntries(basket).pipe(
          map(entries => {
            const lineEntries = this.groupBasketEntries(recipes, entries);
            const lines = this.recipesAndLineEntriesToBasketPreviewLine(recipes, lineEntries);
            this.basketPreview$.next(lines);
            this.basketStats$.next(this.buildStats(lines));
          })
        );
      })
    ).subscribe();

    return this.basketPreview$;
  }

  private fetchBasketEntries(basket: Basket): Observable<BasketEntry[]> {
    if (!basket || !basket.completion || basket.completion.total < 1) {
      this._entries$.next([]);
      return this._entries$;
    }

    const pagesCount = Math.ceil(basket.completion.total / BasketsService.entriesPerPages);
    return forkJoin(Array.apply(null, Array(pagesCount)).map((v, i) => {
      return this.fetchBasketEntriesPage(basket, i + 1);
    })).pipe(
      map((collections: DocumentCollection<BasketEntry>[]) => {
        const entries = flatten(collections.map(col => col.data));
        entries.forEach(e => e.setItemsSelection());
        this._entries$.next(entries);
        return this._entries$.value;
      })
    );
  }

  private buildStats(lines: BasketPreviewLine[]): BasketStats {
    const entriesFound: BasketEntry[] = unionBy(... lines.map(line => line.entries.found), 'id');

    const totalPrice = entriesFound.reduce((total , entry) => {
      const item = entry.attributes['basket-entries-items'].find(i => i.item_id === entry.attributes['selected-item-id']);
      total +=  (entry.attributes.quantity || 0) * parseFloat(item['unit-price']);
      return total;
    }, 0 );

    return {
      recipesCount: lines.length,
      entriesCount: entriesFound.length,
      totalPrice
    };
  }

  private fetchBasketEntriesPage(basket: Basket, page: number) {
    let url = [e.miamAPI, 'api/v1/baskets', basket.id, 'basket-entries'].join('/');
    url = `${url}?include=groceries-entry,items&page[number]=${page}&page[size]=${BasketsService.entriesPerPages}`;

    return this.http.get(url).pipe(
      skipWhile((resp: DocumentCollection<BasketEntry>) => resp.is_loading),
      map((resp: DocumentCollection<BasketEntry>) => {
        const basketEntries = this.basketEntriesService.newCollection();
        basketEntries.fill(resp);
        return basketEntries;
      })
    );
  }

  private recipesAndLineEntriesToBasketPreviewLine(recipes: Recipe[], lineEntries: LineEntries[]): BasketPreviewLine[] {
    return recipes.map((recipe, idx) => {
      const itemsCount = lineEntries[idx].found.length;
      let recipePrice = 0.0;
      const guests = recipe.modifiedGuests || parseInt(recipe.guests, 10);

      lineEntries[idx].found.forEach(entry => {
        const selectedItem = entry.attributes['basket-entries-items'].find(item => {
          return item.item_id === entry.attributes['selected-item-id'];
        });
        const qty = entry.attributes.quantity;
        const unitPrice = parseFloat(selectedItem['unit-price']);
        const numberOfRecipesForEntry = entry.attributes['recipe-ids'].length || 1;

        recipePrice += (qty * unitPrice) / numberOfRecipesForEntry;
      });
      const pricePerGuest = recipePrice / guests;

      return BasketPreviewLine.fromRecipe(recipe, itemsCount, pricePerGuest, recipePrice, lineEntries[idx]);
    });
  }

  private groupBasketEntries(recipes: Recipe[], entries: BasketEntry[]): LineEntries[] {
    return recipes.map(recipe => {
      const lineEntries = { found: [], notFound: [], oftenDeleted: [], removed: [] } as LineEntries;
      entries
        .filter(entry => entry.attributes['recipe-ids'].includes(parseInt(recipe.id, 10)))
        .forEach(entry => {
          const available = entry.attributes['selected-item-id'];
          if (available) {
            switch (entry.attributes['groceries-entry-status']) {
              case 'often_deleted': {
                lineEntries.oftenDeleted.push(entry);
                break;
              }
              case 'deleted': {
                lineEntries.removed.push(entry);
                break;
              }
              default: {
                lineEntries.found.push(entry);
                break;
              }
            }
          } else {
            lineEntries.notFound.push(entry);
          }
        });
      return lineEntries;
    });
  }
}
