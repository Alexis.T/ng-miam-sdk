import { Injectable } from '@angular/core';
import { DocumentCollection, Service } from 'ngx-jsonapi';
import { BehaviorSubject, Observable } from 'rxjs';
import { skipWhile } from 'rxjs/operators';
import { RecipeLike } from '../_models/recipe-like';

@Injectable({
  providedIn: 'root'
})
export class RecipeLikesService extends Service<RecipeLike> {

  public resource = RecipeLike;
  public type = 'recipe-likes';
  public activeRecipeLikes = new Map<string, BehaviorSubject<RecipeLike>>();

  constructor() {
    super();
    this.register();
  }

  getLike(recipeId: string): BehaviorSubject<RecipeLike> {
    this.addLikeToActivesIfNecessary(recipeId);
    return this.activeRecipeLikes.get(recipeId);
  }

  updateLike(recipeId: string, recipeLike: RecipeLike) {
    recipeLike.save().subscribe(() => {
      this.activeRecipeLikes.get(recipeId).next(recipeLike);
    });
  }

  addLikeToActivesIfNecessary(recipeId: string): void {
    if (!this.activeRecipeLikes.get(recipeId)) {
      this.activeRecipeLikes.set(recipeId, new BehaviorSubject(this.new()));
      this.fetchLikesOfRecipe(recipeId).subscribe(likes => {
        if (likes.data[0]) { this.activeRecipeLikes.get(recipeId).next(likes.data[0]); }
      });
    }
  }

  removeLikeFromActivesIfNecessary(recipeId: string) {
    if (this.activeRecipeLikes.get(recipeId).observers.length === 0) {
      this.activeRecipeLikes.delete(recipeId);
    }
  }

  // Because of API implementation, shouldn't return more than 1 recipe-like
  private fetchLikesOfRecipe(recipeId: string): Observable<DocumentCollection<RecipeLike>>{
    return this.all({ remotefilter: { recipe_id: recipeId, is_past: 'true,false' } }).pipe(
      skipWhile(resp => resp.is_loading)
    );
  }
}
