
import { Injectable } from '@angular/core';
import { Service } from 'ngx-jsonapi';
import { Calendar } from '../_models/calendar';

@Injectable({
  providedIn: 'root'
})
export class CalendarsService extends Service<Calendar> {

  public resource = Calendar;
  public type = 'calendars';

  constructor() {
    super();
    this.register();
  }
}