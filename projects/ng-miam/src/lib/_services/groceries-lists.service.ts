import { Inject, Injectable } from '@angular/core';
import { GroceriesList } from '../_models/groceries-list';
import { Service } from 'ngx-jsonapi';
import { skipWhile, tap } from "rxjs/operators";
import { MIAM_BASKET_PATH } from "../providers";
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { AnalyticsService } from './analytics.service';
import { v4 as uuidv4 } from 'uuid'

// @dynamic
@Injectable({
  providedIn: 'root'
})
export class GroceriesListsService extends Service<GroceriesList> {

  resource = GroceriesList;
  type = 'groceries-lists';
  list$ = new BehaviorSubject<GroceriesList>(null);
  private currentSubscription: Subscription;

  constructor(
    @Inject(MIAM_BASKET_PATH) private miamBasketURL: string,
    private analyticsService: AnalyticsService
  ) {
    super();
    this.register();
    this.refreshCurrentList();
  }

  refreshCurrentList(): Observable<GroceriesList> {
    console.log('[Miam] refreshing gl')
    const cachedListId = localStorage.getItem('_miam/listId');
    // adding this uuid will force request to be sent even if one is still in process
    // think about making a queue to process request in order
    const base_route = 'current?uniq_token=' + uuidv4();
    const endpoint = cachedListId ? base_route + '&merge_with=' + cachedListId : base_route;
    // alway keep only the last subscription, that will make forget the last calls
    if (this.currentSubscription) {
      this.currentSubscription.unsubscribe();
    }
    this.currentSubscription = this.get(endpoint).pipe(
      skipWhile(l => l.is_loading),
      tap(l => this.assignList(l))
    ).subscribe();

    return this.list$;
  }

  resetList(): Observable<GroceriesList> {
    return this.get('reset').pipe(
      skipWhile(l => l.is_loading),
      tap(l => this.assignList(l))
    );
  }

  appendRecipeToList(recipeId: string, guests: number, openBasket: boolean = false): Observable<GroceriesList> {
    const list = this.list$.value;
    if (list) {
      if (!list.hasRecipe(recipeId)) {
        // append to list
        list.attributes['append-recipes'] = true;
        const info = { id: recipeId };
        if (guests) {
          Object.assign(info, { guests });
        }
        list.attributes['recipes-infos'] = [info];
        this.analyticsService.sendEvent('add-recipe');
      } else if (guests) {
        // update guests number
        const info = list.recipeInfos.find(i => i.id === recipeId);
        if (info.guests.toString() === guests.toString()) {
          // Avoid saving the list if nothing changed
          return this.list$;
        }
        info.guests = guests.toString();
      }
      delete(list.attributes['recipes-ids']);
      this.saveList(list, openBasket);
    }

    return this.list$;
  }

  removeRecipeFromList(recipeId: string): Observable<GroceriesList> {
    const list = this.list$.value;
    if (list.attributes['recipes-infos']) {
      list.attributes['recipes-infos'] = list.attributes['recipes-infos'].filter(info => {
        return info.id !== recipeId;
      });
      delete(list.attributes['recipes-ids']);
      this.analyticsService.sendEvent('remove-recipe');
      this.saveList(list);
    }

    return this.list$;
  }

  updateRecipeGuests(recipeId: string, guests: number): Observable<GroceriesList>  {
    const list = this.list$.value;
    if (list.attributes['recipes-infos']) {
      const recipeInfo = list.attributes['recipes-infos'].find(info => {
        return info.id === recipeId;
      });
      recipeInfo.guests = guests;
      this.analyticsService.sendEvent('change-guests');
      this.saveList(list);
    }

    return this.list$;
  }

  removeRecipesFromList(): Observable<GroceriesList> {
    const list = this.list$.value;
    list.attributes['recipes-infos'] = [];
    delete(list.attributes['recipes-ids']);
    this.analyticsService.sendEvent('remove-all-recipes');
    this.saveList(list);

    return this.list$;
  }

  private saveList(list: GroceriesList, openBasket: boolean = false) {
    delete(list.attributes['recipes-ids']);
    list.save().pipe(
      tap(() => {
        this.list$.next(list);
        if (openBasket) {
          window.open(`${this.miamBasketURL}/${list.id}`, '_blank');
        }
      })
    ).subscribe();
  }

  private assignList(l){
    if (l.attributes['user-id']) {
      localStorage.removeItem('_miam/listId');
    } else {
      localStorage.setItem('_miam/listId', l.id);
    }
    this.list$.next(l);
  }
}
