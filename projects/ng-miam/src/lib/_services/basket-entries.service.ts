import { Injectable } from '@angular/core';
import { Service } from 'ngx-jsonapi';
import { BasketEntry } from '../_models/basket-entry';
import { ItemsService}  from './items.service';
import { of, Observable } from 'rxjs';
import { tap, skipWhile } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BasketEntriesService extends Service<BasketEntry> {
  public resource = BasketEntry;
  public type = 'basket-entries';

  // private _entries = [];

  constructor( private itemsService: ItemsService) {
    super();
    this.register();
  }

  // DEPRECATED ?
  // getOrFetch(entryId, opts: any = {}): Observable<BasketEntry> {
  //   const entry = this._entries.find(e => e.id === entryId);
  //   if (entry && !opts.reload) {
  //     return of(entry);
  //   } else {
  //     delete(opts.reload);
  //     return this.get(entryId, opts).pipe(
  //       skipWhile(e => e.is_loading),
  //       tap(e => this._entries.push(e))
  //     );
  //   }
  // }
}
