import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DocumentResource, Service } from 'ngx-jsonapi';
import { forkJoin, Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { environment } from '../environment';
import { Ingredient } from '../_models/ingredient';
import { Recipe } from '../_models/recipe';

const MIAM_API_HOST = `${environment.miamAPI}/api/v1/`;

@Injectable({
  providedIn: 'root'
})
export class IngredientsService extends Service<Ingredient> {

  private reviewedNames: Array<string>;

  constructor(private http: HttpClient) {
    super();
    this.register();
  }
  public resource = Ingredient;
  public type = 'ingredients';

  /**
   * Check if ingredient is already known in backend
   * @param ingredientDef exemple : 'steak'
   */
  public isReviewedName(ingredientDef: string): Observable<boolean> {
    if (!this.reviewedNames) {
      // get all knowed ingredient in backend
      return this.http.get<string[]>(`${MIAM_API_HOST}ingredients-definitions/reviewed_names`).pipe(switchMap(reviewedNames => {
        this.reviewedNames = reviewedNames;
        // check if ingredientDef is in the list
        return of( this.reviewedNames.find(el => el === ingredientDef) != undefined);
      }));
    }
    return of(this.reviewedNames.find(el => el === ingredientDef) != undefined);
  }

  public formatIngredients(listIngredient: string[]): Observable<Ingredient[]> {
    return forkJoin(listIngredient.map(el => this.get(`from_s?ingredient_str=${el}`)));
  }

  /**
   * Merge Recipe Ingredients list with output of recipe forms ingredient list
   * it's link with RecipeFormComponent
   * @param recipe to update
   * @param ingredients list of ingredients 
   */
  public createOrUpdateIngredients(recipe: Recipe, ingredients: Ingredient[]): Ingredient[] {

    const oldIngredients = [...recipe.relationships[`ingredients`].data];
    const ingredientsToSave = [];

    ingredients.forEach(ing => {

      if (ing.id) {
        const oldIndex = oldIngredients.findIndex(el => el.id === ing.id);
        const oldIngredientAttribute = oldIngredients[oldIndex].attributes;
        const hasChange =  oldIngredientAttribute.name !== ing.attributes.name ||
                           oldIngredientAttribute.unit !== ing.attributes.unit ||
                           oldIngredientAttribute.quantity !== ing.attributes.quantity;
        if (hasChange){
          oldIngredients[oldIndex].attributes = { ...oldIngredients[oldIndex].attributes,
            name: ing.attributes.name, unit: ing.attributes.unit, quantity: ing.attributes.quantity };
            ingredientsToSave.push(oldIngredients[oldIndex]);
        }
      } else {
        const newIng = this.new();
        delete newIng.id;
        newIng.attributes = { ...newIng.attributes,
           name: ing.attributes.name, unit: ing.attributes.unit, quantity: '' + ing.attributes.quantity };
        newIng.addRelationship(recipe, 'recipe');
        ingredientsToSave.push(newIng);
      }
    });
    return ingredientsToSave;
  }

  /**
   *  Create or update a list Of Ingredient
   *  it'll create if Ingredient id is null otherwise update it
   *  it's perform in parallel , Observable'll complet when all ingredents have been upserted
   * @param ingredients array of Ingredient
   */
  public upsertIngredientList(ingredients: Ingredient[]): Observable<DocumentResource<Ingredient>[]> {
    return forkJoin(
      ingredients.map(
        (ingredient: Ingredient) => {
          return ingredient.save() as Observable<DocumentResource<Ingredient>>;
        }
      )
    );
  }

  public deleteIngredientList(ingredientIds: string[]): Observable<void[]> {
    return forkJoin(
      ingredientIds.map(ingredientId => this.delete(ingredientId))
    );
  }
}
