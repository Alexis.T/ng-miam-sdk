export class BasketEntriesItem {
  item_id: string;
  score: number;
  learning_factor: number;
  times_selected: number;
  selected: boolean;
  default: boolean;
  unit_price: string;
  currency: string;
  pft_plages: number[];
  pft_checksum: string;

  constructor(attrs = {}) {
    Object.assign(this, attrs);
    if (!this.times_selected) this.times_selected = 0
  }
}