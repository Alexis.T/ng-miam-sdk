export class User {
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  picture: string;
  from_user_id: string;
  app_metadata: {
    current_groceries_list_id: string,
    recipe_provider: string,
    permissions: string[],
    roles: string[]
  };
  user_metadata: any;
  forbidProfiling = false;

  constructor(attrs = {}) {
    Object.assign(this, attrs);
  }
}