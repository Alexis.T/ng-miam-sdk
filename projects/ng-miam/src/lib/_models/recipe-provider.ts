import {DocumentCollection, Resource} from "ngx-jsonapi";
import {Recipe} from "./recipe";

export class RecipeProvider extends Resource {
  public attributes = {
    'name': 'NoName'
  };

  public relationships = {
    'recipes': new DocumentCollection<Recipe>()
  }

}
