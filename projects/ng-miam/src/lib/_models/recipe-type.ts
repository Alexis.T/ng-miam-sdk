import {Resource} from "ngx-jsonapi";

export class RecipeType extends Resource {
  public attributes = {
    'name': ''
  };

  get name(): string {
    return this.attributes.name;
  }
}
