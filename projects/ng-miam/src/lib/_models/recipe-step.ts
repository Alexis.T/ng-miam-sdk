import { Resource, DocumentResource } from 'ngx-jsonapi';
import { Recipe } from './recipe';

export class RecipeStep extends Resource {
  public attributes = {
    'step-number': 0,
    'title': '',
    'description': '',
    'media-url': null
  };

  relationships = {
    recipe: new DocumentResource<Recipe>()
  };
  newStep: any;
}
