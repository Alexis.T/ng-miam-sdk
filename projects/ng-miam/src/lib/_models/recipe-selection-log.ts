import { Resource } from 'ngx-jsonapi';

export class RecipeSelectionLog extends Resource {
  attributes = {
    date: '',
    'user-id': '',
    'recipes-infos': []
  };

  get date(): Date {
    return new Date(this.attributes.date);
  }

  get recipesInfos(): { id, guests }[] {
    return this.attributes['recipes-infos'];
  }
}
