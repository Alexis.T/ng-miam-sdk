import { DocumentCollection, Resource, DocumentResource } from 'ngx-jsonapi';
import { sum, normalize, parse, toMinutes, Duration } from 'duration-fns';
import { RecipeProvider } from './recipe-provider';
import { RecipeStatus } from './recipe-status';
import { RecipeType } from './recipe-type';
import { RecipeStep } from './recipe-step';
import { Ingredient } from './ingredient';
import { Sponsor } from './sponsor';

interface SimplifiedIngredient {
  name: string;
  qty: number;
  unit: string;
}


export class Recipe extends Resource {

  attributes = {
    title: '',
    'ext-id': '',
    'ext-link': '',
    description: '',
    'number-of-guests': '',
    'ingredients-str': [],
    'preparation-time': '',
    'preheating-time': '',
    'cooking-time': '',
    'resting-time': '',
    'media-url': '',
    source: '',
    'informational-page-html': '',
    'filigrane-logo-url': '',
    'informational-sentence': '',
    difficulty: 1,
    cost: 1,
    suggested: false,
    popularity: 0,
    'video-id': '',
    promoted: false,
    shared: 'not_shared'
  };

  relationships = {
    ingredients: new DocumentCollection<Ingredient>(),
    'recipe-steps': new DocumentCollection<RecipeStep>(),
    'recipe-provider': new DocumentResource<RecipeProvider>(),
    'recipe-status': new DocumentResource<RecipeStatus>(),
    'recipe-type': new DocumentResource<RecipeType>(),
    sponsors: new DocumentCollection<Sponsor>()
  };

  ttl = 3600; // recipes will be cached for 1 hour

  selected: boolean;
  finalTimer = this.attributes['preparation-time'] + this.attributes['preheating-time'] +
    this.attributes['cooking-time'] + this.attributes['resting-time'];
  modifiedGuests: number;
  private _eventsGroupId: string;
  private _eventOrigin: string;
  private _normalizedIngredients: SimplifiedIngredient[];

  shallowCopyWithEvents(origin, groupId): Recipe {
    let res = new Recipe();
    Object.assign(res, this);
    res._eventOrigin = origin;
    res._eventsGroupId = groupId;
    return res;
  }

  get eventsGroupId(): string {
    return this._eventsGroupId;
  }

  get eventOrigin(): string {
    return this._eventOrigin;
  }

  get title(): string {
    return this.attributes.title;
  }

  get promoted(): boolean {
    return this.attributes.promoted;
  }

  get guests(): string {
    return this.attributes['number-of-guests'];
  }

  get shared(): boolean {
    return this.attributes.shared === 'shared';
  }

  set shared(isShared: boolean) {
    this.attributes.shared = isShared ? 'shared' : 'not_shared';
  }

  get filigraneLogoUrl(): String {
    if (this.attributes['filigrane-logo-url']) {
      return this.attributes['filigrane-logo-url']
    }
    if (this.sponsors.length > 0){
      return this.sponsors[0].attributes['logo-url']
    }
    return null
  }

  get informationalSentence(): String {
    return this.attributes['informational-sentence']
  }

  get costLabel(): string {
    switch (this.attributes.cost) {
      case 1: {
        return 'faible';
      }
      case 2: {
        return 'moyen';
      }
      case 3: {
        return 'élevé';
      }
      default: {
        return 'moyen';
      }
    }
  }

  get costPicture(): string {
    return `assets/images/card-recipe/cost/${this.costLabel.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}.svg`;
  }

  get difficultyLabel(): string {
    switch (this.attributes.difficulty) {
      case 1: {
        return 'facile';
      }
      case 2: {
        return 'moyen';
      }
      case 3: {
        return 'difficile';
      }
      default: {
        return 'facile';
      }
    }
  }

  get difficultyPicture(): string {
    return `assets/images/card-recipe/difficulty/${this.difficultyLabel.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}.svg`;
  }

  get totalTime(): string {
    const duration = normalize(sum(
      parse(this.attributes['preparation-time']),
      parse(this.attributes['preheating-time']),
      parse(this.attributes['cooking-time']),
      parse(this.attributes['resting-time'])
    ));

    if (toMinutes(duration) < 10) {
      return ' < 10 min.';
    }

    return this.formatDuration(duration);
  }


  getPhotos() {
    return [this.attributes['media-url']];
  }

  get preparationTime() {
    return this.formatDuration(normalize(parse(this.attributes['preparation-time'])));
  }

  get cookingTime() {
    return this.formatDuration(normalize(parse(this.attributes['cooking-time'])));
  }

  get restingTime() {
    return this.formatDuration(normalize(parse(this.attributes['resting-time'])));
  }

  // Returns ingredients, putting the sponsored ones first 
  get ingredients(): Ingredient[] {
    return this.relationships.ingredients.data
      .filter((i: Ingredient) => i.attributes.active)
      .sort((i1, i2) => {
        const isI1Forced = i1.attributes['forced-eans'] && i1.attributes['forced-eans'].length > 0;
        const isI2Forced = i2.attributes['forced-eans'] && i2.attributes['forced-eans'].length > 0;
        return (isI1Forced && !isI2Forced) ? -1 : 1;
      });
  }

  get sponsors(): Sponsor[] {
    return this.relationships?.sponsors?.data;
  }
  get steps(): RecipeStep[] {
    return this.relationships['recipe-steps'].data.sort((a, b) => a.attributes['step-number'] > b.attributes['step-number'] ? 1 : -1);
  }

  get status(): RecipeStatus {
    return this.relationships && this.relationships['recipe-status'] && this.relationships['recipe-status'].data;
  }

  get recipeType(): RecipeType {
    return this.relationships && this.relationships['recipe-type'] && this.relationships['recipe-type'].data;
  }

  get modifiedIngredients(): SimplifiedIngredient[] {
    return this.normalizedIngredients.map(ingredient => {
      return { ...ingredient, qty: ingredient.qty * (this.modifiedGuests || +this.guests) };
    });
  }

  get normalizedIngredients(): SimplifiedIngredient[] {
    if (!this._normalizedIngredients) {
      this._normalizedIngredients = this.ingredients.map((ingredient: Ingredient) => {
        return {
          name: ingredient.attributes.name,
          qty: (+ingredient.quantity) / (+this.guests || 1),
          unit: ingredient.attributes.unit
        };
      });
    }
    return this._normalizedIngredients;
  }

  private formatDuration(duration: Duration) {
   if (!toMinutes(duration)){
    return '-';
   }
   if (duration.days) { duration.hours += 24 * duration.days; }

   if (!duration.hours) {
     return `${duration.minutes} min.`;
   }
   let formatedDuration = `${duration.hours} h`;
   if (duration.minutes) {
    formatedDuration += ` ${duration.minutes}`;
   }
   return formatedDuration;
  }
}
