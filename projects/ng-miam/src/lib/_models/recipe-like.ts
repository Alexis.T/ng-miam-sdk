import { DocumentResource, Resource } from 'ngx-jsonapi';
import { Recipe } from './recipe';

export class RecipeLike extends Resource {
  public attributes = {
    'user-id': '',
    'is-past': true
  };

  public relationships = {
    recipe: new DocumentResource<Recipe>()
  };

  public get isPast(): boolean {
    return this.attributes['is-past'];
  }
  public set isPast(value: boolean) {
    this.attributes['is-past'] = value;
  }
  public toggle() {
    this.isPast = !this.isPast;
  }
}
