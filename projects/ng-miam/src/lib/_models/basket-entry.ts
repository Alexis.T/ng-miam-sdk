import { Resource, DocumentCollection, DocumentResource } from "ngx-jsonapi";
import { Item } from "./item";
import { GroceriesEntry } from "./groceries-entry";
import { BasketEntriesItem } from "./basket-entries-item";

export class BasketEntry extends Resource {
  replacing: boolean = false;
  selectedItem: Item;
  candidateItems: Item[];
  candidateIndex = 0;
  totalPrice: string;
  showCandidates = false;

  // TODO: basket-entries-items should be a meta, not an attribute (extend ngx-jsonapi)
  attributes = {
    'selected-item-id': '',
    'learning-factor': 1,
    'basket-entries-items': new Array<BasketEntriesItem>(0),
    'quantity': 1,
    'recipe-ids': [],
    'groceries-entry-status': 'active'
  };

  relationships = {
    'items': new DocumentCollection<Item>(),
    'groceries-entry': new DocumentResource<GroceriesEntry>()
  };

  get entryName(): string {
    return this.relationships['groceries-entry']?.data?.attributes.name;
  }

  get groceriesEntry(): GroceriesEntry {
    return this.relationships['groceries-entry']?.data;
  }

  get groceriesEntryStatus(): string {
    return this.attributes['groceries-entry-status'];
  }

  get quantity(): number {
    return this.attributes.quantity;
  }

  set quantity(qty) {
    this.attributes.quantity = qty;
  }

  getItems(): Item[] {
    return this.relationships['items'].data.map(item => {
      const beItem = this.attributes["basket-entries-items"].find(i => i.item_id == item.id);
      item.attributes["unit-price"] = beItem['unit-price'];
      item.attributes.currency = beItem.currency;
      item.variableCapacity = beItem.pft_plages && beItem.pft_plages[0]?.toString();
      return item;
    });
  }

  getGroceriesEntry(): GroceriesEntry {
    return this.relationships['groceries-entry'].data;
  }

  setItemsSelection() {
    this.selectedItem = this.getItems().find((i: Item) => {
      return this.attributes['selected-item-id'] && (i.id == this.attributes['selected-item-id'].toString());
    });
    if (!this.selectedItem) return;
    this.candidateItems = this.getItems().filter(
      (i: Item) => i.id != this.selectedItem.id
    );
  }

  updateTotalPrice() {
    if (!this.selectedItem) return;
    const unitPrice = this.selectedItem.getPrice();
    const quantity = this.attributes.quantity;
    this.totalPrice = (unitPrice * quantity).toFixed(2);
  }

  selectItem(id: string) {
    this.attributes["selected-item-id"] = id;
    this.setItemsSelection();
    this.updateTotalPrice();
    this.showCandidates = false;
  }

  swipeCandidate(indexIncrement: number) {
    const newIndex = (this.candidateIndex + indexIncrement);
    if (newIndex < 0 || newIndex > (this.candidateItems.length - 3)) return;
    else this.candidateIndex = newIndex;
  }

  toggleShowCandidates() {
    this.showCandidates = !this.showCandidates;
  }
}