import { Resource } from 'ngx-jsonapi';

export class RecipeStatus extends Resource {
  public attributes = {
    name: '',
    label: ''
  };

  public get hidden(): boolean {
    return this.name === 'DELETED';
  }

  public get name(): string {
    return this.attributes.name;
  }

  public get label(): string {
    return this.attributes.label;
  }

  public get icon(): string {
    switch (this.attributes.name) {
      case 'DRAFT': {
        return 'text_snippet';
      }
      case 'SUBMITTED': {
        return 'schedule';
      }
      case 'ACTIVE': {
        return 'done';
      }
      case 'CHANGE_REQUIRED': {
        return 'error_outline';
      }
      case 'INACTIVE': {
        return 'highlight_off';
      }
      default: {
        return 'help_outline';
      }
    }
  }

  public get color(): string {
    switch (this.attributes.name) {
      case 'DRAFT': {
        return null;
      }
      case 'SUBMITTED': {
        return 'accent';
      }
      case 'ACTIVE': {
        return 'primary';
      }
      case 'CHANGE_REQUIRED': {
        return 'warn';
      }
      case 'INACTIVE': {
        return null;
      }
      default: {
        return null;
      }
    }
  }
}
