import {DocumentResource, Resource} from "ngx-jsonapi";
import { RecipeProvider } from './recipe-provider';
import { RecipeStatus } from './recipe-status'

export class Package extends Resource {
  public attributes = {
    title: '',
    system: ''
  };

  get title(): string {
    return this.attributes.title;
  }

  relationships = {
    provider: new DocumentResource<RecipeProvider>(),
    status: new DocumentResource<RecipeStatus>()
  };
}