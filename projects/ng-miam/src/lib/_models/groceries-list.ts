import { DocumentCollection, Resource } from "ngx-jsonapi";
import { GroceriesEntry } from "./groceries-entry";
import { Recipe } from "./recipe";

export class GroceriesList extends Resource {
  public attributes = {
    name: '',
    'created-at': '',
    'updated-at': '',
    entries: [],
    'recipes-ids': [],
    'recipes-infos': [],
    'search-options': {},
    settings: {},
    'user-id': '',
    'append-recipes': false
  };

  public relationships = {
    'groceries-entries': new DocumentCollection<GroceriesEntry>()
  };

  get recipeInfos(): { id: string, guests: string }[] {
    return this.attributes['recipes-infos'];
  }

  hasRecipe(recipeId: string): boolean {
    return  this.recipeInfos?.map(info => info.id).includes(recipeId);
  }

  guestsForRecipe(recipe: Recipe): number {
    return +this.recipeInfos.find(info => info.id === recipe.id)?.guests;
  }
}
