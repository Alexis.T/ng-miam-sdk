import { Recipe } from './recipe';
import { BasketEntry } from './basket-entry';

export interface LineEntries {
  found: BasketEntry[];
  notFound: BasketEntry[];
  oftenDeleted: BasketEntry[];
  removed: BasketEntry[];
}

export class BasketPreviewLine {
  id?: string;
  record?: any;
  isRecipe: boolean;
  inlineTag?: string;
  title: string;
  picture: string;
  description: string[];
  price: string;
  count: number;
  entries?: LineEntries;
  private _displayMode = false; // hide "remove", show "add" buttons + click on the line trigger recipe details display

  static fromRecipe(
    recipe: Recipe,
    itemsCount: number,
    pricePerGuest: number = null,
    recipePrice: number = null,
    entries: LineEntries = null
  ): BasketPreviewLine {
    const line = new BasketPreviewLine();
    Object.assign(line, {
      id: recipe.id,
      isRecipe: true,
      title: recipe.attributes.title,
      picture: recipe.attributes['media-url'],
      description: [`${itemsCount} article${itemsCount > 1 ? 's' : ''}`],
      count: recipe.modifiedGuests || recipe.guests,
      entries
    });

    if (pricePerGuest) {
      line.description.push(`${pricePerGuest.toFixed(2)} € / personne`);
    }
    if (recipePrice !== undefined && recipePrice !== null) {
      line.price = `${recipePrice.toFixed(2)}`;
    }

    return line;
  }

  static fromBasketEntry(entry: BasketEntry): BasketPreviewLine {
    const line = new BasketPreviewLine();
    const item = entry.relationships.items.data.find(i => i.id === entry.attributes['selected-item-id'].toString());
    const beItem = entry.attributes['basket-entries-items'].find(i => i.item_id === entry.attributes['selected-item-id']);
    const price = parseFloat(beItem['unit-price']) * entry.attributes.quantity;
    const gEntry = entry.relationships['groceries-entry'].data;
    const recipesCount = gEntry && gEntry.attributes['recipe-ids'] && gEntry.attributes['recipe-ids'].length;
    if (item && beItem && price != null && price != undefined) {
      Object.assign(line, {
        id: entry.id,
        record: entry,
        isRecipe: false,
        inlineTag: recipesCount && recipesCount > 1 ? `Pour ${recipesCount} recettes` : null,
        title: entry.entryName,
        picture: item.attributes.image,
        description: [`${item.attributes.brand ? item.attributes.brand : ''} ${item.attributes.name} | ${item.capacity}`],
        price: `${price.toFixed(2)}`,
        count: entry.attributes.quantity
      });
    }
    return line;
  }

  hasEntries(): boolean {
    return this.entries && (
      this.entries.found.length > 0 ||
      this.entries.notFound.length > 0 ||
      this.entries.oftenDeleted.length > 0 ||
      this.entries.removed.length > 0
    );
  }

  get displayMode(): boolean {
    return this._displayMode
  }

  set displayMode(mode: boolean) {
    if (mode) {
      this.description[0] = this.description[0].replace('article', 'ingrédient');
    }
    this._displayMode = mode;
  }
}
