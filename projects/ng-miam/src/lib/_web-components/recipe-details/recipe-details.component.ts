import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input, ChangeDetectorRef, Output, EventEmitter, OnChanges, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { Recipe } from '../../_models/recipe';
import { Icon } from '../../_types/icon.enum'
import { ContextService, GroceriesListsService, PointOfSalesService, RecipeEventsService, UserService } from '../../_services';
import { forkJoin, Subscription } from 'rxjs';
import { RecipeLike } from '../../_models/recipe-like';
import { stopEventPropagation } from '../../_utils';
import { take } from 'rxjs/operators';

@Component({
  selector: 'ng-miam-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeDetailsComponent implements OnChanges {
  @Input() recipe: Recipe;
  @Input() previewAllowed = true;
  @Output() recipeAdded: EventEmitter<void> = new EventEmitter();
  @Output() recipeChanged: EventEmitter<void> = new EventEmitter();
  @Output() recipeError: EventEmitter<void> = new EventEmitter();

  public tab = 0;
  public showDetail = true;
  public steps: any[] = [];
  public activeStep = 0;
  public icon = Icon;
  public ingredientsChecked = {};
  protected subscriptions: Subscription[];

  constructor(
    public cdr: ChangeDetectorRef,
    public groceriesListsService: GroceriesListsService,
    public userService: UserService,
    protected posService: PointOfSalesService,
    private recipeEventsService: RecipeEventsService,
    private contextService: ContextService) {
    this.subscriptions = [];
  }

  ngOnChanges(): void {
    if (this.recipe) {
      this.steps = this.recipe.steps;
      this.cdr.detectChanges();
    }
  }

  changeActiveStep(index: number) {
    this.activeStep = index;
    this.cdr.detectChanges();
  }

  addRecipe(): void {
    forkJoin([
      this.userService.isLogged$.asObservable().pipe(take(1)),
      this.posService.isPosValid().pipe(take(1))
    ]).subscribe(res => {
      const isHookOk = this.contextService.hookCallback(res[0], res[1]);
      if (isHookOk) {
        this.addRecipeActionOK();
      } else {
        this.addRecipeActionKO();
      }
    });
  }

  addRecipeActionOK(): void {
    if (!this.groceriesListsService.list$.value.hasRecipe(this.recipe.id)){
      this.recipeEventsService.sendEvent(this.recipe, this.recipeEventsService.ACTION_ADDED)
    }
    this.groceriesListsService.appendRecipeToList(this.recipe.id, this.recipe.modifiedGuests);
    this.recipeAdded.emit();
  }

  addRecipeActionKO(): void {
    localStorage.setItem('_miam/cached-recipe', JSON.stringify(this.recipe));
    this.recipeError.emit();
  }

  updateGuests(guests) {
    this.recipe.modifiedGuests = guests;
    this.recipeChanged.emit();
    this.cdr.detectChanges();
  }

  print() {
    window.print();
  }

  openCalendar(event: Event) {
    stopEventPropagation(event);
    // TODO implement
  }

  shareRecipe(event: Event) {
    stopEventPropagation(event);
    // TODO implement
  }

  toggleAddon(){
    this.showDetail =  !this.showDetail;
    this.cdr.detectChanges();
  }
}
