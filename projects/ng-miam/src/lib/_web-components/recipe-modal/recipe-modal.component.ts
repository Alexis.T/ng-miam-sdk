import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { filter } from 'rxjs/operators';
import { Recipe } from '../../_models';
import { GroceriesListsService, RecipesService } from '../../_services';
import { PrintService } from '../../_services/print.service';
import { RecipeDetailsComponent } from '../recipe-details/recipe-details.component';

@Component({
  selector: 'ng-miam-recipe-modal',
  templateUrl: './recipe-modal.component.html',
  styleUrls: ['./recipe-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeModalComponent implements OnInit {
  recipe: Recipe;
  show = false;
  previewMode = false;
  previewAllowed: boolean;


  @ViewChild('details') detailsComponent: RecipeDetailsComponent;

  constructor(
    private cdr: ChangeDetectorRef,
    public recipesService: RecipesService,
    private printService: PrintService,
    private elRef: ElementRef,
    private groceriesListsService: GroceriesListsService
  ) { }

  ngOnInit(): void {
    this.recipesService.displayedRecipe$.pipe(
      filter(result => !!result?.recipe)
    ).subscribe(result => {
      this.recipe = result.recipe;
      this.previewMode = result.previewMode;
      this.previewAllowed = result.previewAllowed;
      this.show = true;
      this.printService.prepareForPrint(this.elRef);
      this.detailsComponent?.cdr.markForCheck();
      if (this.detailsComponent) { this.detailsComponent.showDetail = true; }
      this.cdr.detectChanges();
    });
  }

  hide() {
    this.show = false;
    this.previewMode = false;
    this.recipesService.hide();
    this.printService.dissmissPrinting();
    this.cdr.detectChanges();
  }

  togglePreviewMode() {
    this.previewMode = !this.previewMode;
    this.cdr.detectChanges();
  }

  removeRecipe() {
    this.groceriesListsService.removeRecipeFromList(this.recipe.id);
    this.hide();
  }

  updateRecipeGuests(guests: number) {
    this.recipe.modifiedGuests = guests;
  }

  // If the recipe is already in basket, update the number of guests (= append recipe to list again)
  recipeChanged() {
    if (this.groceriesListsService.list$.value.hasRecipe(this.recipe.id)) {
      this.groceriesListsService.appendRecipeToList(this.recipe.id, this.recipe.modifiedGuests);
    }
    this.recipesService.displayedRecipeChanged.emit();
  }
}
