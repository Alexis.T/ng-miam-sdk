import { Component, ViewEncapsulation, ChangeDetectionStrategy, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild,
  ChangeDetectorRef } from '@angular/core';
import { BasketPreviewLine } from '../../_models/basket-preview-line';
import { Item } from '../../_models/item';
import { Icon } from '../../_types/icon.enum';
import { BasketsService } from '../../_services';
import { switchMap, take } from 'rxjs/operators';
import { AnalyticsService } from '../../_services/analytics.service';

@Component({
  selector: 'ng-miam-replace-item',
  templateUrl: './replace-item.component.html',
  styleUrls: ['./replace-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ReplaceItemComponent implements OnInit {
  @Input() line: BasketPreviewLine;
  @Output() selected = new EventEmitter<string>();

  public items: Array<Item>;
  public currentItem: any;
  public icon = Icon;

  constructor(
    private cdr: ChangeDetectorRef,
    private basketsService: BasketsService,
    private analyticsService: AnalyticsService
  ) { }

  @ViewChild('itemsList', { static: true }) itemsListElement: ElementRef;

  ngOnInit(): void {
    this.initLine();
  }

  onClose(): void {
    this.selected.emit();
  }

  scrollItemsList(event: WheelEvent) {
    this.itemsListElement.nativeElement.scrollLeft += 2 * event.deltaY;
    event.preventDefault();
  }

  public onSelectItem(item: Item) {
    this.line.record.selectItem(item.id);
    this.analyticsService.sendEvent('replace-item');
    this.line.record.save().pipe(
      switchMap(() => this.basketsService.loadBasket()),
      take(1)
    ).subscribe();
    this.selected.emit(this.line.id);
  }

  private initLine(): void {
    this.currentItem = this.line.record.attributes['basket-entries-items'][0];
    this.items = this.line.record.relationships.items.data.filter((item: Item) => item.id === this.currentItem.item_id);
  }

  priceIntegerPart(price: string ){
    const values = price.split('.');
    return values[0];
  }

  /**
   * @param price item?.attributes["unit-price"] is a string
   * and we wan two decimal (not 0.8 or 0.800) 
   * @returns first two decimal
   */
  priceDecimalPart(price: string){
    const values = price.split('.');
    // force 2 decimals
    return values?.length === 2 ? (values[1] + '00').slice(0, 2) : '00';
  }
}
