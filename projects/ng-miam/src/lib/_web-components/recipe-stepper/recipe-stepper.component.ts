import { ChangeDetectionStrategy, Component, ElementRef, HostListener, QueryList, ViewChild, ViewChildren, ViewEncapsulation} from '@angular/core';
import { RecipeFormComponent } from '../recipe-form/recipe-form.component';


@Component({
  selector: 'ng-miam-recipe-stepper',
  templateUrl: './recipe-stepper.component.html',
  styleUrls: ['./recipe-stepper.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeStepperComponent extends RecipeFormComponent {


  INGREDIENT_PLACEHOLDER = 'Exemple :\n500g de farine\n250g de beurre\nhuile d\'olive\n4 tranches de jambon';
  STEP_PLACEHOLDER = `Exemple :\nHacher grossièrement le céleri et l'oignon.\nCouper le chou-fleur en petits bouquets.`;
  INGREDIENT_INSTRUCTIONS = 'Vous pouvez saisir un ingrédient ou copier coller une liste d’ingrédients. \n Une ligne correspond à un ingrédient.';
  STEP_INSTRUCTIONS = 'Vous pouvez saisir une étape ou copier coller une liste d’étapes.\n Une ligne correspond à une étape.';

  currentStep: number;
  bigImageWarning = false;

  @ViewChild('stepperLinks') stepperLinks: ElementRef;
  @ViewChildren('step')  steps: QueryList<ElementRef>;

  @HostListener('window:scroll', ['$event'])
  onScroll() {
    if (this.stepperLinks && this.steps.length > 0) {
      const sections = this.steps.toArray();
      sections.forEach((section, index) => {
        // offset between top of screen and form start
        const currentPosition = document.documentElement.scrollTop;
        // header is sticky and stay on screen
        // We are in the section if position is more than the (diff between top of page and top of section) - (header height)
        const relativeSectionTop = section.nativeElement[`offsetTop`] - (this.stepperLinks.nativeElement[`offsetHeight`] + 10);
        // and less than the (diff between top of page and top of section) + (section height)
        const relativeSectionBottom = section.nativeElement[`offsetTop`] + section.nativeElement[`offsetHeight`] -
        (this.stepperLinks.nativeElement[`offsetHeight`] + 10);
        const isSelected = currentPosition >= relativeSectionTop  && currentPosition < relativeSectionBottom;
        if (isSelected){
          this.currentStep = index;
          this.cdr.detectChanges();
        }
      });
    }
  }

  scroolToAnchor(index: number){
    if (this.stepperLinks && this.steps.length > 0) {
    const step = this.steps.toArray()[index].nativeElement;
    const header = this.stepperLinks.nativeElement;
    window.scrollTo({top: step.offsetTop - header.offsetHeight, behavior: 'smooth'});
    }
  }

  changeGuest(newGestNumber: number){
    if (+newGestNumber !== this.recipeForm?.get('attributes')?.get('number-of-guests').value ){
      this.recipeForm?.get('attributes')?.get('number-of-guests').patchValue(+newGestNumber);
      this.recipeForm?.get('attributes')?.get('number-of-guests').markAsDirty();
    }
  }

  displayBigFileWarning(display: boolean) {
    this.bigImageWarning = display;
    this.cdr.detectChanges();
  }
}
