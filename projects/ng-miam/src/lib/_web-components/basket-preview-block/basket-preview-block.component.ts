import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter,
  ViewChildren
} from '@angular/core';
import { Subscription, BehaviorSubject, combineLatest } from 'rxjs';
import { GroceriesListsService } from '../../_services/groceries-lists.service';
import { BasketsService, BasketStats } from '../../_services/baskets.service';
import { BasketPreviewLine } from '../../_models/basket-preview-line';
import { skipWhile, tap, map } from 'rxjs/operators';
import remove from 'lodash-es/remove';
import { BasketPreviewLineComponent } from '../basket-preview-line/basket-preview-line.component';
import { PointOfSalesService } from '../../_services/point-of-sales.service';
import { ContextService } from '../../_services/context.service';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-basket-preview-block',
  templateUrl: './basket-preview-block.component.html',
  styleUrls: ['./basket-preview-block.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BasketPreviewBlockComponent implements OnInit, OnDestroy {
  @Input() blockTitle = 'Mes repas';
  @Input() recipeId: string;
  @Input() showNotice = false;
  @Output() statsUpdated = new EventEmitter<BasketStats>();
  @Output() guestCountChanged = new EventEmitter<number>();
  @ViewChildren(BasketPreviewLineComponent) lineComponents: BasketPreviewLineComponent[];

  public showLines = true;
  public lines: BasketPreviewLine[];
  public loading = false;
  corsIssues = false;
  icon = Icon;

  blockStates: BehaviorSubject<{ currentOpenLine: string, overlayOpen: boolean, lineComponent: BasketPreviewLineComponent }>;

  private subscriptions = [];
  private linesHashes = {};

  constructor(
    private cdr: ChangeDetectorRef,
    public listsService: GroceriesListsService,
    public posService: PointOfSalesService,
    private contextService: ContextService,
    public basketsService: BasketsService,
  ) { }

  ngOnInit() {
    this.blockStates = new BehaviorSubject({ currentOpenLine: null, overlayOpen: false, lineComponent: null });
    this.subscriptions = [
      this.contextService.getCORSIssuesState().subscribe(res => {
        this.corsIssues = res;
        this.cdr.detectChanges();
      }),
      this.blockStates.subscribe(newState => this.handelblockState(newState)),
      combineLatest([this.listsService.list$, this.posService.pos$]).pipe(
        skipWhile(results => !results[0] || !results[1]),
        tap(results => {
          const list = results[0];
          this.loading = list.attributes['recipes-infos'] && list.attributes['recipes-infos'].length > 0;
          this.cdr.detectChanges();
        })
      ).subscribe(),
      this.basketsService.basketPreview$.pipe(
        map(preview => (preview && this.recipeId) ? preview.filter(line => line.id === this.recipeId) : preview),
        skipWhile(preview => !preview || preview.length === 0),
        tap(preview => this.refreshPreview(preview))
      ).subscribe(() => {
        this.loading = false;
        this.cdr.detectChanges();
      }),
      this.basketsService.basketStats$.pipe(
        tap(stats => this.statsUpdated.emit(stats))
      ).subscribe()
    ];
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  toggleLines() {
    this.showLines = !this.showLines;
    this.cdr.detectChanges();
  }

  removeRecipe(line: BasketPreviewLine) {
    this.listsService.removeRecipeFromList(line.id);
  }

  updateRecipe(line: BasketPreviewLine) {
    this.listsService.updateRecipeGuests(line.id, line.count);
    this.guestCountChanged.emit(line.count);
  }

  closeOverlay() {
    if (this.blockStates.getValue() && this.blockStates.getValue().lineComponent) {
      this.blockStates.value.lineComponent.productChoosen();
    }
  }

  handelblockState(state: { currentOpenLine: string, overlayOpen: boolean, lineComponent: BasketPreviewLineComponent }) {
    this.cdr.detectChanges();
  }

  private refreshPreview(newLines: BasketPreviewLine[]) {
    if (!this.lines) {
      // First load
      this.lines = newLines;
    } else {
      const newLinesIds = [];
      newLines.forEach((line: BasketPreviewLine) => {
        newLinesIds.push(line.id);
        const existingLine = this.lines.find(l => l.id === line.id);
        if (existingLine) {
          // Update existing object with its new values
          Object.assign(existingLine, line);

          // Mark basket lines component for check
          const existingComp = this.lineComponents.find(comp => comp.line.id === line.id);
          existingComp.cdr.markForCheck();

          // Force reload the basket entries if their quantities or selected items have changed
          const entries = line.entries.found.sort(e => parseInt(e.id, 10));
          const hash = entries.map(e => e.attributes['selected-item-id'] + '-' + e.attributes.quantity).join('/');
          if (hash !== this.linesHashes[line.id] && existingComp.entriesLines$.value) {
            existingComp.buildEntriesLines();
            this.linesHashes[line.id] = hash;
          }
        } else {
          // Push the new line to the array
          this.lines.push(line);
        }
      });
      // Remove deleted lines
      remove(this.lines, l => !newLinesIds.includes(l.id));
    }
    this.cdr.detectChanges();
  }
}
