import {
  Component, ChangeDetectionStrategy, ViewEncapsulation, ChangeDetectorRef,
  OnDestroy, Input, ViewChildren, ElementRef, QueryList, AfterViewInit, SimpleChanges, OnChanges
} from '@angular/core';
import { BasketsService } from '../../_services/baskets.service';
import { Subscription } from 'rxjs';
import { Icon } from '../../_types/icon.enum';
import { BasketPreviewLine } from '../../_models';
import { ContextService } from '../../_services';
import groupBy from 'lodash-es/groupBy';


@Component({
  selector: 'ng-miam-recipe-tags',
  templateUrl: './recipe-tags.component.html',
  styleUrls: ['./recipe-tags.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeTagsComponent implements OnChanges, AfterViewInit, OnDestroy {

  @Input() extId: string;
  @Input() recipeDetailPreviewAllowed = true;
  @ViewChildren('tag') tags: QueryList<ElementRef>;

  icon = Icon;
  recipePreviewLines: BasketPreviewLine[];
  wrappedItemsCount: number;
  rowCount: number;
  lastItemIndexOfFirstRow: number;
  isListOpen: boolean;

  protected subscriptions: Subscription[];

  constructor(
    private cdr: ChangeDetectorRef,
    private basketService: BasketsService,
    private contexteservice: ContextService,
  ) {
    this.subscriptions = [];
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (simpleChanges.hasOwnProperty('extId') && simpleChanges.extId.firstChange) {
      this.subscriptions.push(this.basketService.basketPreview$.subscribe(basketPreviewLines => {
        if (basketPreviewLines) {
          // we get the list of basketPreviewLines (at recipe level) witch have items link with given extId
          this.recipePreviewLines = basketPreviewLines.filter(bpl => {
            return bpl.entries.found.find(entry => entry.selectedItem.attributes['ext-id'] === this.extId);
          });
          this.cdr.detectChanges();
        }
      }
      ));
    }
    this.cdr.detectChanges();
  }

  ngAfterViewInit(): void {
    // Call each time list of recipe-tags change
    this.subscriptions.push(this.tags.changes.subscribe(queryList => this.detectWrappedTags(queryList)));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  /**
   * This function detect and count wrapped item below the fist row
   * items and container can have various width so row number can differ
   * base on absolute position in window we are able to find element witch belongs
   * to the first row and other and then count hidden elements and number of rows
   * @param tags  QueryList<ElementRef>
   */
  detectWrappedTags(tags: QueryList<ElementRef>) {

    // get for each element his absolute position from top of window
    const tagsPosition = tags.map(tag => tag.nativeElement.getBoundingClientRect());

    // group by top , element that share the same top position are in the same line
    const tagsGroupByRow = groupBy(tagsPosition, 'top');

    // get list of distinct hight
    const tagsDistinctHight = Object.keys(tagsGroupByRow);

    this.rowCount = tagsDistinctHight.length;

    // get size of first row 'll help us to get last item of this row
    this.lastItemIndexOfFirstRow = tagsGroupByRow[tagsDistinctHight[0]].length - 1;

    // wrapped items count is length - tags count in first row
    this.wrappedItemsCount = tags.toArray().length - (this.lastItemIndexOfFirstRow + 1);
    this.cdr.detectChanges();
  }

  toggleRecipe(id: string) {
    this.contexteservice.miam.recipes.display(id, this.recipeDetailPreviewAllowed );
  }

  toggleList() {
    this.isListOpen = !this.isListOpen;
    this.cdr.detectChanges();
  }
}
