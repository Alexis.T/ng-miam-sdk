import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ng-miam-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
  @Input() wide = false;

  constructor() { }
}
