import { Component, Output, EventEmitter, ChangeDetectionStrategy, ViewEncapsulation, ChangeDetectorRef, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { RecipesService } from '../../_services';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-recipe-helper',
  templateUrl: './recipe-helper.component.html',
  styleUrls: ['./recipe-helper.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeHelperComponent implements OnInit, OnDestroy {
  @Input() stringDetailLocation = 'dans l\'onglet Mes Repas';
  @Output() close = new EventEmitter();
  @Output() moreInfo = new EventEmitter();

  icon = Icon;
  isDisplayed = false;
  protected subscriptions: Subscription[];

  constructor(
    private cdr: ChangeDetectorRef,
    private recipesService: RecipesService
  ) {
    this.subscriptions = [];
   }

  ngOnInit() {
    this.subscriptions.push(this.recipesService.displayHelper$.subscribe(displayed => {
      this.isDisplayed = displayed;
      document.body.style.overflow = displayed ? 'hidden' : 'auto';
      this.cdr.detectChanges();
    }));
  }

  onClose() {
    this.recipesService.toggleHelper();
    this.close.emit();
  }

  onMoreInfo() {
    this.moreInfo.emit();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
