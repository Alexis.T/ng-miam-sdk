import { Component, ViewEncapsulation, ChangeDetectorRef, ChangeDetectionStrategy, Inject, OnInit, ElementRef, HostListener, ViewChild, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { skipWhile } from 'rxjs/operators';
import { RecipesService, GroceriesListsService, ContextService, PointOfSalesService, RecipeEventsService, UserService } from '../../_services';
import { AnalyticsService } from '../../_services/analytics.service';
import { DisplayRecipeCardComponent } from '../display-recipe-card/display-recipe-card.component';
import { v4 as uuidv4 } from 'uuid';
import { Icon } from '../../_types/icon.enum';

/**
 * This is an angular component design to be a web component
 * that's why we use onPushStrategie with the changeDetetectorRef
 * to avoid common issue
 * guide -> https://netbasal.com/a-comprehensive-guide-to-angular-onpush-change-detection-strategy-5bac493074a4
 * and alsow we use ShadowDom to protect style of our component
 * and alsow style of the parent aplication that will use it
 * doc -> https://angular.io/api/core/ViewEncapsulation
 */
@Component({
  selector: 'ng-miam-catalog-recipe-card',
  templateUrl: './catalog-recipe-card.component.html',
  styleUrls: ['./catalog-recipe-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CatalogRecipeCardComponent extends DisplayRecipeCardComponent implements OnInit, AfterViewInit {
  private eventsGroupId = uuidv4();
  @Output() actionTriggered = new EventEmitter<string>();

  @ViewChild('miamMoreActions') moreActions: ElementRef;
  actionsOpened = false;

  @HostListener('window:scroll') onScroll(e: Event): void {
    this.sendShowEvent('show-recipe-card-catalog');
  }

  constructor(
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
    @Inject(RecipesService) recipeService: RecipesService,
    @Inject(RecipeEventsService) recipeEventsService: RecipeEventsService,
    @Inject(GroceriesListsService) groceriesListsService: GroceriesListsService,
    @Inject(UserService) userService: UserService,
    @Inject(PointOfSalesService) pointOfSalesService: PointOfSalesService,
    @Inject(ContextService) contextService: ContextService,
    @Inject(AnalyticsService) analyticsService: AnalyticsService,
    protected element: ElementRef
    ) {
    super(cdr, recipeService, recipeEventsService, groceriesListsService, userService, pointOfSalesService, contextService,
      analyticsService, element);
  }

  ngOnInit() {
    this.subscriptions.push(this.groceriesListsService.list$.pipe(
      skipWhile(l => !l)
    ).subscribe(() => this.cdr.detectChanges()));
    if (this.recipe) {
      this.recipe = this.recipe.shallowCopyWithEvents(this.recipeEventsService.ORIGIN_CATALOG, this.eventsGroupId);
      this.recipeEventsService.sendEvent(this.recipe, this.recipeEventsService.ACTION_PROPOSED);
    }
  }

  ngAfterViewInit() {
    // TODO: remove when actions for non-personal recipes are implemented, necessary for the injector to differenciate the 2 cases
    if (this.recipe.relationships['recipe-provider']?.data.id === 'personal') {
      this.moreActions.nativeElement.style.visibility = 'visible';
    }
    else {
      this.moreActions.nativeElement.style.visibility = 'hidden';
    }
  }

  updateGuests(count) {
    this.recipe.modifiedGuests = count;
    if (this.groceriesListsService.list$.value.hasRecipe(this.recipe.id)){
      this.addRecipe(false);
    }
    this.cdr.detectChanges();
  }

  openMoreActions(event) {
    event.preventDefault();
    event.stopPropagation();
    if (this.recipe.relationships['recipe-provider']?.data.id === 'personal') {
      this.actionsOpened = true;
      this.cdr.detectChanges();
    }
  }

  closeMoreActions() {
    this.actionsOpened = false;
    this.cdr.detectChanges();
  }
}
