import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output,
  ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { Recipe } from '../../_models';
import { RecipesService } from '../../_services';
import { Icon } from '../../_types/icon.enum';


@Component({
  selector: 'ng-miam-catalog-header',
  templateUrl: './catalog-header.component.html',
  styleUrls: ['./catalog-header.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CatalogHeaderComponent implements OnDestroy, OnInit, OnChanges {
  @Output() returnButtonPressed = new EventEmitter<void>();
  @Output() searchStringChanged = new EventEmitter<string>();
  @Input() forcedRecipe: Recipe;
  @Input() forcedPictureUrl = '';
  @Input() displayReturnButton = false;
  @Input() title = '';

  searchString = '';
  subscriptions: Subscription[];
  defaultRecipe: Recipe;
  isLoading: boolean;
  icon = Icon;

  constructor(
    private recipesService: RecipesService,
    private cdr: ChangeDetectorRef) {
    this.subscriptions = [];
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.subscriptions.push(this.recipesService.getRandom({number: 1, size: 1}, { suggested: true })
      .subscribe((result: Recipe[]) => {
        this.defaultRecipe = result[0];
        this.isLoading = false;
        this.cdr.detectChanges();
      })
    );
  }

  ngOnChanges() {
    this.cdr.detectChanges();
  }

  openRecipe() {
    if (this.forcedRecipe) {
      this.recipesService.displayObject(this.forcedRecipe,  { guests : this.forcedRecipe.modifiedGuests });
    }
    else {
      this.recipesService.displayObject(this.defaultRecipe, { guests : this.defaultRecipe.modifiedGuests });
    }
  }

  // Not used in component, but can be called by parent via ViewChild
  resetSearchString() {
    this.searchString = '';
    this.cdr.detectChanges();
  }

  emitSearchString() {
    this.searchStringChanged.emit(this.searchString);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
