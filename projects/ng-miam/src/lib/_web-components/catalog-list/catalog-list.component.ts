import { Component, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, OnInit, Output, EventEmitter, Input,
  OnChanges } from '@angular/core';
import { DocumentCollection } from 'ngx-jsonapi';
import { BehaviorSubject, Subscription } from 'rxjs';
import { skipWhile} from 'rxjs/operators';
import { Recipe } from '../../_models/recipe';
import { RecipesService } from '../../_services';
import { Icon } from '../../_types/icon.enum';
import { RecipeFilter, RecipeFilters } from '../../_types/recipe-filters';

@Component({
  selector: 'ng-miam-catalog-list',
  templateUrl: './catalog-list.component.html',
  styleUrls: ['./catalog-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CatalogListComponent implements OnDestroy, OnInit, OnChanges {
  @Input() filters: RecipeFilters;
  @Input() noRecipeTextFirstLine: string;
  @Input() noRecipeTextSecondLine: string;
  @Input() insertCreateCard = false;
  @Output() retrievedRecipes = new EventEmitter<Recipe[]>();
  @Output() filterRemoved = new EventEmitter<string>();
  @Output() recipeActionTriggered = new EventEmitter<{action: string, recipe: Recipe}>();

  currentRecipes: Recipe[];
  lastPage = 0;
  subscriptions: Subscription[];
  loading = true;
  icon = Icon;
  bottomAnchor: HTMLElement;
  filtersAreEmpty = true;
  filtersToDisplay = [];

  private remoteFilters = {};

  private intersectionObserver: IntersectionObserver;
  private mutationObserver: MutationObserver;

  constructor(
    private recipesService: RecipesService,
    private cdr: ChangeDetectorRef
  ) {
    this.subscriptions = [];
    this.currentRecipes = [];
    this.filters = { search: '' };
  }

  ngOnInit() {
    this.bottomAnchor = document.getElementById('miam-catalog-list__anchor');
    this.initLoadOnScroll();
  }

  ngOnChanges() {
    this.remoteFilters = this.buildRemoteFilters();
    this.filtersToDisplay = this.buildFiltersArray();
    this.reloadPage();
  }

  initLoadOnScroll() {
    // Receive an event each time the anchor enters/leaves the viewport
    this.intersectionObserver = new IntersectionObserver(([entry]) => { this.autoloadRecipeIfNecessary(); }, {});
    // Receive an event each time an attribute/child is added/deleted/updated in the parent div of the recipes
    // i.e. will be triggered each time new recipes are displayed
    this.mutationObserver = new MutationObserver(() => { this.autoloadRecipeIfNecessary(); });

    this.intersectionObserver.observe(this.bottomAnchor);
    this.mutationObserver.observe(
      document.getElementsByClassName('miam-catalog-list__cards')[0],
      { attributes: true, childList: true, subtree: false } // subtree false => changes on children won't trigger the callback
    );
  }

  anchorIsOnScreen() {
    const viewportHeight = window.innerHeight || document.documentElement.clientHeight;
    const rect = this.bottomAnchor.getBoundingClientRect();
    return (rect.top <= viewportHeight && rect.bottom >= 0);
  }

  autoloadRecipeIfNecessary() {
    if (this.anchorIsOnScreen() && !this.loading && this.currentRecipes.length > 0) {
      this.loadMore();
    }
  }

  loadMore() {
    this.loading = true;
    this.lastPage++;
    this.loadRecipesPage(this.lastPage, false);
  }

  loadRecipesPage(page: number, resetRecipes: boolean) {
    this.subscriptions.push(this.recipesService.all({
      remotefilter: this.remoteFilters,
      page: { size: 20, number: page },
      include: ['ingredients', 'recipe-steps', 'recipe-type', 'recipe-status', 'sponsors', 'recipe-provider']
    }).pipe(
      skipWhile(resp => resp.is_loading)
    )
    .subscribe((result: DocumentCollection<Recipe>) => {
      this.currentRecipes = resetRecipes ? result.data : [...this.currentRecipes, ...result.data];
      this.loading = false;
      this.retrievedRecipes.emit(this.currentRecipes);
      this.cdr.detectChanges();
    }));
  }

  removeFilter(filterName) {
    this.filterRemoved.emit(filterName);
  }

  buildRemoteFilters() {
    this.filtersAreEmpty = !(this.filters.search && this.filters.search !== '');
    const difficulty = this.buildRemoteFilter(this.filters.difficulty, '-');
    const cost = this.buildRemoteFilter(this.filters.cost, '-').split('-');
    const totalTime = this.buildRemoteFilter(this.filters.time, ',');

    const filters = {
      ...(difficulty.length > 0) ? { difficulty: difficulty + ',eq' } : {}, // ex: filter[difficulty]=1-2-3,eq => all checked
      ...(cost.length === 2) ? { computed_cost: cost[0] + ',gt,' + cost[1] + ',lt' } : {}, // ex: filter[cost]=5,gt,10,lt => 5 < cost < 10
      ...(totalTime.length > 0) ? { 'total-time': totalTime } : {},
      ...(this.filters.additionalFilters?.filters) ? this.filters.additionalFilters.filters : {},
      ...(this.filters.search) ? {search: this.filters.search } : {}
    };
    return filters;
  }

  // Reads the values of a field and converts it to the string to pass in the remoteFilters
  // ex: buildRemoteFilter({
  //   {name: '1', value: true, text: 'Facile'},
  //   {name: '2', value: false, text: 'Moyen'},
  //   {name: '3', value: true, text: 'Difficile'}
  // }, '-')  ==> '1-3'
  buildRemoteFilter(filter: RecipeFilter[], separator: string) {
    let result = '';
    for (const f of filter) {
      if (f.value) {
        if (result.length > 0) { result += separator; }
        result += f.name;
        this.filtersAreEmpty = false;
      }
    }
    return result;
  }

  buildFiltersArray(): string[] {
    let filterNames = [];
    filterNames = [...filterNames, ...this.getSelectedFilters(this.filters.cost), ...this.getSelectedFilters(this.filters.difficulty),
      ...this.getSelectedFilters(this.filters.time)];
    if (this.filters.additionalFilters && this.filters.additionalFilters.title !== '') {
      filterNames = [this.filters.additionalFilters.title].concat(filterNames);
    }
    return filterNames;
  }

  getSelectedFilters(filtersArray): string[] {
    return filtersArray.filter(el => el.value).map(el => el.text);
   }

  reloadPage() {
    this.lastPage = 1;
    this.loading = true;
    this.loadRecipesPage(this.lastPage, true);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.intersectionObserver.disconnect();
    this.mutationObserver.disconnect();
  }
}
