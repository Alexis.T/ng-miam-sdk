import { Component, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, Inject } from '@angular/core';
import { SuppliersService, PackageService, RecipeTypeService, SponsorService, UserService, RecipesService } from '../../_services';
import { RecipeCatalogComponent } from '../recipe-catalog/recipe-catalog.component';

const CONNECTED_FIRST_LINE = 'Désolé vous n’avez pas encore de recettes favorites';
const NOT_CONNECTED_FIRST_LINE = 'Désolé, vous devez être connecté pour visualiser vos recettes favorites';

@Component({
  selector: 'ng-miam-favorite-page',
  templateUrl: './favorite-page.component.html',
  styleUrls: ['./favorite-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FavoritePageComponent extends RecipeCatalogComponent implements OnInit {

  homePageTitle = 'Vos recettes favorites';
  noRecipeTextFirstLine = CONNECTED_FIRST_LINE;
  displayHomePage = false;
  forcedPictureUrl = 'https://storage.googleapis.com/assets.miam.tech/generic/catalog/recettes%20persos.jpg';

  constructor(
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
    @Inject(RecipeTypeService) recipeTypesService: RecipeTypeService,
    @Inject(SponsorService) sponsorService: SponsorService,
    @Inject(SuppliersService) supplierService: SuppliersService,
    @Inject(PackageService) packageService: PackageService,
    @Inject(RecipesService) recipesService: RecipesService,
    private usersService: UserService) {
    super(cdr, recipeTypesService, sponsorService, supplierService, packageService, recipesService);
  }

  ngOnInit() {
    this.resetFilters();
    this.usersService.isLogged$.subscribe(logged => {
      this.noRecipeTextFirstLine = logged ? CONNECTED_FIRST_LINE : NOT_CONNECTED_FIRST_LINE;
      this.cdr.detectChanges();
    });
  }

  // Used on load to build the filters attribute, and on click on the link to reset the filters
  resetFilters() {
    super.resetFilters();
    this.filters.additionalFilters = { filters: { liked: true, active: 'true,false' }, title: '' };
    this.cdr.detectChanges();
  }

  returnToHomePage() {
    // Do not return to home page
  }
}
