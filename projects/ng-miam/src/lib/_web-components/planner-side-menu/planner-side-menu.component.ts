import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { skipWhile } from 'rxjs/operators';
import { CalendarEvent } from '../../_models/calendar-event';
import { RecipesService } from '../../_services';
import { CalendarEventsService } from '../../_services/calendar-events.service';



@Component({
  selector: 'ng-miam-planner-side-menu',
  templateUrl: './planner-side-menu.component.html',
  styleUrls: ['./planner-side-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlannerSideMenuComponent implements OnInit {

  events: CalendarEvent[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private recipesService: RecipesService,
    private calendarEventsService: CalendarEventsService
  ) { }

  ngOnInit(): void {

    // To test that we can create some custom events with a recipe that are draggable, we load a bunch of recipes,
    // transform them into events and display them. Later PRs will not initialise events like this.
    this.recipesService.all().pipe(
      skipWhile(res => res.is_loading)
    ).subscribe(results => {
      results.data.forEach(recipe => {
        const event = this.calendarEventsService.new();
        event.addRelationship(recipe, 'recipe');
        this.events.push(event);
      });
      this.cdr.detectChanges();
    });
  }
}
