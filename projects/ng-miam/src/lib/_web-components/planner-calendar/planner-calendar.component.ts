import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { skipWhile } from 'rxjs/operators';
import { CalendarEvent } from '../../_models/calendar-event';
import { RecipesService } from '../../_services';
import { CalendarEventsService } from '../../_services/calendar-events.service';

@Component({
  selector: 'ng-miam-planner-calendar',
  templateUrl: './planner-calendar.component.html',
  styleUrls: ['./planner-calendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlannerCalendarComponent implements OnInit, OnDestroy{
  viewDate: Date = new Date();
  events: CalendarEvent[] = [];
  refresh: Subject<any> = new Subject();

  private subscriptions: Array<Subscription> = [];
  constructor(
    private calendarEventsService: CalendarEventsService,
    private recipesService: RecipesService,
    private cdr: ChangeDetectorRef
  ) { }
  ngOnInit(): void {
    this.subscriptions.push(
      // TODO: for the moment we initialize the calendar with some recipes all on the same day
      // as for now, if we don't do this, the space where the event are displayed doesn't appear
      this.recipesService.all().pipe(
        skipWhile(res => res.is_loading)
      ).subscribe(results => {
        results.data.forEach(recipe => {
          const event = this.calendarEventsService.new();
          event.addRelationship(recipe, 'recipe');
          event.start = new Date();
          this.events.push(event);
        });
        this.refresh.next();
        this.cdr.detectChanges();
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  eventDropped(event: CalendarEvent, newStart: Date): void {
    if (!event.attributes['event-date']) {
      const eventToAdd = this.calendarEventsService.new();
      eventToAdd.addRelationship(event.recipe, 'recipe');
      this.events.push(eventToAdd);
      this.moveEvent( eventToAdd, newStart);
    } else {
      this.moveEvent(event, newStart);
    }
  }

  moveEvent(event: CalendarEvent, newStart: Date): void {
    event.start = newStart;
    this.events = [...this.events];
    this.refresh.next();
    this.cdr.detectChanges();
  }
}
