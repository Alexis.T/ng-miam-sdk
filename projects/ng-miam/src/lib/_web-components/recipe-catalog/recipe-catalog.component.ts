import { Component, ViewEncapsulation, ChangeDetectionStrategy, ViewChild, AfterViewInit, ChangeDetectorRef, OnInit, ElementRef,
  ViewChildren, QueryList } from '@angular/core';
import { skipWhile, take } from 'rxjs/operators';
import { CatalogCategoryComponent } from '../catalog-category/catalog-category.component';
import { Recipe } from '../../_models';
import { RecipeTypeService, SponsorService, PackageService, SuppliersService, RecipesService } from '../../_services';
import { Icon } from '../../_types/icon.enum';
import { CatalogHeaderComponent } from '../catalog-header/catalog-header.component';
import { RecipeFilter, RecipeFilters } from '../../_types/recipe-filters';
import { forkJoin } from 'rxjs';


@Component({
  selector: 'ng-miam-recipe-catalog',
  templateUrl: './recipe-catalog.component.html',
  styleUrls: ['./recipe-catalog.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeCatalogComponent implements AfterViewInit, OnInit {
  @ViewChild(CatalogHeaderComponent) catalogHeader: CatalogHeaderComponent;
  @ViewChild('miamCatalogTopAnchor') topAnchor: ElementRef;

  @ViewChildren(CatalogCategoryComponent)  categoriesComp!: QueryList<CatalogCategoryComponent>;


  filters: RecipeFilters;
  homePageTitle = 'Qu\'est-ce qui vous ferait plaisir ?';
  titleInResearchMode = 'Résultats de recherche';
  displayHomePage = true;
  isFilterCollapsed = false;
  returnTopButtonShouldBeDisplayed = false;
  icon = Icon;
  categories = [];
  recipeInHeader: Recipe;
  creatingRecipe = false;
  recipeToUpdate: Recipe;

  constructor(
    public cdr: ChangeDetectorRef,
    public recipeTypesService: RecipeTypeService,
    public sponsorService: SponsorService,
    public supplierService: SuppliersService,
    public packageService: PackageService,
    private recipesService: RecipesService
  ) {}

  ngOnInit() {
    this.resetFilters();

    forkJoin([
      this.recipeTypesService.getTypes().pipe(
        skipWhile(types => types.length === 0),
        // recipeTypesService.getTypes() comes from a behavior subject, it does not emit the complete() function
        // you absolutly need a take(1) or call complete yourself to trigger the forkjoin subscribe
        take(1)
      ),
      this.supplierService.supplier$.asObservable().pipe(
        skipWhile(supp => supp === undefined || supp == null),
        take(1)
      )
    ]).subscribe((res) => {
      const supplier = res[1];
      this.packageService.categories_for([supplier.id]).pipe(
        skipWhile(packages => packages.is_loading)
      ).subscribe((packages) => {
        this.initCategories(packages);
        this.selectCategoryFromUrl();
        this.cdr.detectChanges();
      })
    });
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  initCategories(packages) {
    this.categories.push({
      key: 'suggestions',
      title: 'Suggestions de la semaine',
      filters: { suggested: true }
    });
    this.categories.push({
      key: 'partenaires',
      title: 'Recettes de nos partenaires',
      filters: { sponsors: true }
    });
    packages.data.forEach(categoryPackage => {
      this.categories.push({
        key: categoryPackage.title,
        title: categoryPackage.title,
        filters: { packages: categoryPackage.id }
      });
    });
    this.categories.push({
      key: 'entrees',
      title: 'Entrées',
      filters: { recipe_type_id: this.recipeTypesService.getTypeByName('entrée').id }
    });
    this.categories.push({
      key: 'plats',
      title: 'Plats',
      filters: { recipe_type_id: this.recipeTypesService.getTypeByName('plat principal').id }
    });
    this.categories.push({
      key: 'desserts',
      title: 'Desserts',
      filters: { recipe_type_id: this.recipeTypesService.getTypeByName('dessert').id }
    });
  }

  selectCategoryFromUrl() {
    const paramsString = window.location.search;
    const paramsArray = new URLSearchParams(paramsString);
    const selected_category_key = paramsArray.get('catalogCategory');
    if (!selected_category_key){
      return;
    }
    const selected_category = this.categories.find((cat) => cat.key === selected_category_key);
    if (!selected_category){
      return;
    }
    this.categoryTitleClicked(selected_category);
  }

  // Used on load to build the filters attribute, and on click on the link to reset the filters
  resetFilters() {
    this.filters = {
      // type: [
      //   {name: recipeTypesService.getType(1).id, value: false, text: 'Entrées'},
      //   {name: recipeTypesService.getType(2).id, value: false, text: 'Plats'},
      //   {name: recipeTypesService.getType(3).id, value: false, text: 'Desserts'}
      // ],
      difficulty: [
        {name: '1', value: false, text: 'Facile'},
        {name: '2', value: false, text: 'Moyenne'},
        {name: '3', value: false, text: 'Difficile'}
      ],
      cost: [
        {name: '0-5', value: false, text: 'Bas (≈ ≤ 5€)'},
        {name: '5-10', value: false, text: 'Moyen (≈ 5-10€) '},
        {name: '10-1000', value: false, text: 'Élevé (≈ ≥ 10€) '}
        // we set a limit at 1000€ max, if a recipe cost more should be non sense anyway
      ],
      time: [
        {name: '15', value: false, text: '15m'},
        {name: '30', value: false, text: '30m'},
        {name: '60', value: false, text: '1h'},
        {name: '120', value: false, text: '2h'}
      ],
    };
    if (this.catalogHeader) { this.catalogHeader.resetSearchString(); }
    this.filters.search = '';
    if (!this.displayHomePage){ this.returnToHomePage(); }
  }

  updateSearch(search: string) {
    if (search !== this.filters.search) {
      this.filters.search = search;
      this.reloadList();
    }
  }

  updateFilters(filters: RecipeFilters) {
    this.filters = { ...filters }; // Necessary to trigger the ngOnChanges
    this.reloadList();
  }

  reloadList() {
    // If there are no filters nor search, return to home page
    if (this.areFiltersEmpty()) { this.returnToHomePage(); }
    else { // Reload the list
      this.filters = { ...this.filters }; // Necessary to trigger the ngOnChanges
      this.displayHomePage = false;
      this.cdr.detectChanges();
    }
  }

  categoryTitleClicked(event: {title: string, filters: any}) {
    this.returnToTop();
    this.filters.additionalFilters = { filters: event.filters, title: event.title };
    this.reloadList();
  }

  areFiltersEmpty(): boolean {
    return this.isFieldEmpty(this.filters.difficulty) && this.isFieldEmpty(this.filters.cost) && this.isFieldEmpty(this.filters.time)
      && this.filters.search === '' && !this.filters.additionalFilters?.filters;
  }

  isFieldEmpty(filter: RecipeFilter[]) {
    for (const f of filter) {
      if (f.value) {
        return false;
      }
    }
    return true;
  }

  removeFilter(filterText: string) {
    if (filterText === this.filters.additionalFilters?.title) {
      this.filters.additionalFilters = undefined;
    }
    else {
      for ( const filterType in this.filters) {
        // additionalFilters isn't an array, thus isn't iterable
        if (this.filters.hasOwnProperty(filterType) && filterType !== 'additionalFilters') {
          for (const filterField of this.filters[filterType]) {
            if (filterField.text === filterText) { filterField.value = false; }
          }
        }
      }
    }
    this.reloadList();
  }

  returnToTop() {
    if (this.topAnchor) {
      this.topAnchor.nativeElement.scrollIntoView();
    }
  }

  displayReturnTopButton() {
    this.returnTopButtonShouldBeDisplayed = this.topAnchor.nativeElement.getBoundingClientRect().bottom < -400;
    this.cdr.detectChanges();
  }

  changeHeaderImage(recipes: Recipe[]) {
    if (recipes.length > 0 && this.filters.additionalFilters?.filters && !this.recipeInHeader) {
      this.recipeInHeader = recipes[0];
      this.cdr.detectChanges();
    }
  }

  returnToHomePage() {
    this.displayHomePage = true;
    this.recipeInHeader = undefined; // Don't force a recipe in header on homePage
    this.resetFilters();
    this.cdr.detectChanges();
  }

  onFilterCollapsed() {
    this.isFilterCollapsed = !this.isFilterCollapsed;
    this.cdr.detectChanges();
    // to trigger after the detectChanges because we need to alread have the display change to update cards
    this.categoriesComp.forEach( cat => {
      cat.updateWidth();
    });
  }

  goToCreatePage(recipe = null) {
    this.creatingRecipe = true;
    this.recipeToUpdate = recipe;
    this.cdr.detectChanges();
    this.returnToTop();
  }

  leaveCreatePage() {
    this.creatingRecipe = false;
    this.reloadList();
    this.returnToTop();
  }

  manageRecipeAction(event: {action: string, recipe: Recipe}) {
    switch (event.action) {
      case 'CREATE': {
        this.goToCreatePage();
        break;
      }

      case 'EDIT': {
        this.goToCreatePage(event.recipe);
        break;
      }

      case 'DELETE': {
        if (event.recipe?.id && event.recipe.relationships['recipe-provider']?.data.id === 'personal') {
          this.recipesService.delete(event.recipe.id).subscribe(() => {
            this.reloadList();
          });
        }
        break;
      }
    }
  }
}
