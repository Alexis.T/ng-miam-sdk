import { Component, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, Inject } from '@angular/core';
import { Recipe } from '../../_models';
import { PackageService, RecipesService, RecipeTypeService, SponsorService, SuppliersService, UserService } from '../../_services';
import { RecipeCatalogComponent } from '../recipe-catalog/recipe-catalog.component';

const CONNECTED_FIRST_LINE = 'Désolé vous n’avez pas encore de recettes personnelles';
const NOT_CONNECTED_FIRST_LINE = 'Désolé, vous devez être connecté pour visualiser vos recettes personnelles';
const CONNECTED_SECOND_LINE = 'Créez en une dès maintenant !';
const NOT_CONNECTED_SECOND_LINE = 'Faites vos courses à partir de vos propres recettes !';

@Component({
  selector: 'ng-miam-personal-recipes',
  templateUrl: './personal-recipes.component.html',
  styleUrls: ['./personal-recipes.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonalRecipesComponent extends RecipeCatalogComponent implements OnInit {

  homePageTitle = 'Mes recettes personnelles';
  noRecipeTextFirstLine = CONNECTED_FIRST_LINE;
  noRecipeTextSecondLine = CONNECTED_SECOND_LINE;
  displayHomePage = false;
  forcedPictureUrl = 'https://storage.googleapis.com/assets.miam.tech/generic/catalog/recettes%20persos.jpg';

  displayCreateButton = false;
  userIsLogged = false;

  constructor(
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
    @Inject(RecipeTypeService) recipeTypesService: RecipeTypeService,
    @Inject(SponsorService) sponsorService: SponsorService,
    @Inject(SuppliersService) supplierService: SuppliersService,
    @Inject(PackageService) packageService: PackageService,
    @Inject(RecipesService) recipesService: RecipesService,
    private usersService: UserService) {
    super(cdr, recipeTypesService, sponsorService, supplierService, packageService, recipesService);
  }

  ngOnInit() {
    this.resetFilters();
    this.usersService.isLogged$.subscribe(logged => {
      this.userIsLogged = logged;
      this.noRecipeTextFirstLine = logged ? CONNECTED_FIRST_LINE : NOT_CONNECTED_FIRST_LINE;
      this.noRecipeTextSecondLine = logged ? CONNECTED_SECOND_LINE : NOT_CONNECTED_SECOND_LINE;
      this.cdr.detectChanges();
    });
  }

  // Used on load to build the filters attribute, and on click on the link to reset the filters
  resetFilters() {
    super.resetFilters();
    this.filters.additionalFilters = { filters: { personal: true, active: 'true,false' }, title: '' };
    this.cdr.detectChanges();
  }

  returnToHomePage() {
    // Do not return to home page
  }

  isListEmpty(recipes: Recipe[]) {
    this.displayCreateButton = (recipes.length === 0 && this.userIsLogged);
    this.cdr.detectChanges();
  }
}
