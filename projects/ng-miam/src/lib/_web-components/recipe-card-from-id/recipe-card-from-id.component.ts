
import {Component, ViewEncapsulation, ChangeDetectorRef, ChangeDetectionStrategy, Input, OnChanges, Inject,  ElementRef } from '@angular/core';
import { AbstractRecipeCardComponent } from '../../_components/abstracts/abstract-recipe-card.component';
import { PointOfSalesService } from '../../_services/point-of-sales.service';
import { RecipesService, RecipeEventsService, GroceriesListsService, ContextService, UserService } from '../../_services';

import { skipWhile} from 'rxjs/operators';
import { AnalyticsService } from '../../_services/analytics.service';

/**
 * This is an angular component design to be a web component
 * that's why we use onPushStrategie with the changeDetetectorRef
 * to avoid common issue
 * guide -> https://netbasal.com/a-comprehensive-guide-to-angular-onpush-change-detection-strategy-5bac493074a4
 * and alsow we use ShadowDom to protect style of our component
 * and alsow style of the parent aplication that will use it
 * doc -> https://angular.io/api/core/ViewEncapsulation
 */
@Component({
  selector: 'ng-miam-recipe-card-from-id',
  templateUrl: './recipe-card-from-id.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
// TODO rename recipe card to suggestion card
export class RecipeCardFromIdComponent extends AbstractRecipeCardComponent implements OnChanges {

  isloaded = false;

  @Input() recipeId: string;

  constructor(
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
    @Inject(RecipesService) recipeService: RecipesService,
    @Inject(GroceriesListsService) groceriesListsService: GroceriesListsService,
    @Inject(UserService) userService: UserService,
    @Inject(PointOfSalesService) pointOfSalesService: PointOfSalesService,
    @Inject(ContextService) contextService: ContextService,
    protected analyticsService: AnalyticsService,
    public recipeEventsService: RecipeEventsService,
    protected element: ElementRef
  ) {
    super(cdr, recipeService, recipeEventsService, groceriesListsService, userService, pointOfSalesService, contextService,
      analyticsService, element);
  }

  ngOnChanges() {
    if (this.recipeId) {
      this.loadRecipe();
    }
  }

  // If recipe already in basket, update the number of guests so that the pricing gets updated as well
  // (display = false so that the recipe details don't show up)
  guestsChanged() {
    if (this.groceriesListsService.list$.value.hasRecipe(this.recipe.id)){
      this.addRecipe(false);
    }
  }

  private loadRecipe() {
        this.recipeService.get(this.recipeId,
          { include: ['ingredients', 'recipe-steps', 'recipe-status', 'recipe-type', 'sponsors' ] }
          ).pipe(
        skipWhile(result => result.is_loading)
      ).subscribe(result => {
        this.recipe = result;
        this.isloaded = true;
        this.cdr.detectChanges();
      });
  }
}
