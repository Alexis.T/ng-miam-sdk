
import {Component, ViewEncapsulation, ChangeDetectorRef, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges, Inject, ViewChild,
  ElementRef, HostListener } from '@angular/core';
import { AbstractRecipeCardComponent } from '../../_components/abstracts/abstract-recipe-card.component';
import { PointOfSalesService } from '../../_services/point-of-sales.service';
import { RecipesService, RecipeEventsService, GroceriesListsService, ContextService, UserService } from '../../_services';
import { Recipe } from '../../_models/recipe';
import { combineLatest,  Observable } from 'rxjs';
import { GroceriesList } from '../../_models/groceries-list';
import { skipWhile} from 'rxjs/operators';
import { DisplayRecipeCardComponent } from '../display-recipe-card/display-recipe-card.component';
import { AnalyticsService } from '../../_services/analytics.service';
import { v4 as uuidv4 } from 'uuid';


/**
 * This is an angular component design to be a web component
 * that's why we use onPushStrategie with the changeDetetectorRef
 * to avoid common issue
 * guide -> https://netbasal.com/a-comprehensive-guide-to-angular-onpush-change-detection-strategy-5bac493074a4
 * and alsow we use ShadowDom to protect style of our component
 * and alsow style of the parent aplication that will use it
 * doc -> https://angular.io/api/core/ViewEncapsulation
 */
@Component({
  selector: 'ng-miam-recipe-card',
  templateUrl: './recipe-card.component.html',
  styleUrls: ['./recipe-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
// TODO rename recipe card to suggestion card
export class RecipeCardComponent extends AbstractRecipeCardComponent implements OnChanges {
  @Input() randomModeEnable: boolean;
  @Input() context: {
    shelfIngredients: Array<string>, currentIngredients: Array<string>, basketIngredients: Array<string>
  };
  isEmpty = false;
  private eventsGroupId = uuidv4();

  @ViewChild('displayCard') displayCard: DisplayRecipeCardComponent;
  @HostListener('window:scroll') onScroll(e: Event): void {
    this.sendShowEvent();
  }

  constructor(
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
    @Inject(RecipesService) recipeService: RecipesService,
    @Inject(GroceriesListsService) groceriesListsService: GroceriesListsService,
    @Inject(UserService) userService: UserService,
    @Inject(PointOfSalesService) pointOfSalesService: PointOfSalesService,
    @Inject(ContextService) contextService: ContextService,
    protected analyticsService: AnalyticsService,
    public recipeEventsService: RecipeEventsService,
    protected element: ElementRef
  ) {
    super(cdr, recipeService, recipeEventsService, groceriesListsService, userService, pointOfSalesService, contextService,
      analyticsService, element);
  }

  ngOnChanges(simpleChanges: SimpleChanges) {
    if (( simpleChanges.hasOwnProperty('context') && !this.randomModeEnable) || !this.recipe) {
      this.loadRecipe();
    }
  }

  sendDisplayEvent() {
    this.analyticsService.sendEvent('display-recipe');
  }

  // If recipe already in basket, update the number of guests so that the pricing gets updated as well
  // (display = false so that the recipe details don't show up)
  guestsChanged() {
    if (this.groceriesListsService.list$.value.hasRecipe(this.recipe.id)){
      this.addRecipe(false);
    }
  }

  private loadRecipe() {
    this.subscriptions.push(
      combineLatest([
        this.groceriesListsService.list$,
        this.fetchRecipes()
      ]).pipe(
        skipWhile(results => !results[0] || !results[1])
      ).subscribe(results => this.processResults(results[0], results[1]))
    );
  }

  private fetchRecipes(): Observable<Recipe[]> {
    if (this.randomModeEnable) {
      return this.recipeService.getRandom();
    } else {
      return this.recipeService.getSuggestion(
        this.context?.shelfIngredients,
        this.context?.currentIngredients,
        this.context?.basketIngredients,
        this.eventsGroupId
      );
    }
  }

  // If the recipe is in basket, will show number of guests as defined in the basket
  // Otherwise, will show default number of guests for the recipe
  private processResults(list: GroceriesList, recipes: Recipe[]) {
    this.isEmpty = (!recipes || recipes.length === 0);
    if (list && !this.isEmpty) {
      const recipeChanged = this.recipe?.id !== recipes[0]?.id;
      // get the first recipe and shallow copy it to permit to send event and do not disturb any other component
      // if ressource is cached without copy another load will get the attributes
      this.recipe = recipes[0].shallowCopyWithEvents(this.recipeEventsService.ORIGIN_SUGGESTION, this.eventsGroupId);
      this.recipe.modifiedGuests = list.guestsForRecipe(this.recipe) || +this.recipe.guests;
      this.initIngredientsString();
      this.displayCard.cdr.markForCheck();
      if (recipeChanged) {
        this.analyticsEventSent = false;
        this.sendShowEvent();
      }
    } else {
      this.hide.emit();
    }
    this.cdr.detectChanges();
  }
}
