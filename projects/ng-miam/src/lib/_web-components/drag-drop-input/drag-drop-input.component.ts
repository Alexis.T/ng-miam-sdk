import { ElementRef, OnChanges } from '@angular/core';
import { Component, Output, EventEmitter, ChangeDetectorRef, ViewEncapsulation, ChangeDetectionStrategy, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-drag-drop-input',
  templateUrl: './drag-drop-input.component.html',
  styleUrls: ['./drag-drop-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DragDropInputComponent implements OnChanges {

  @Input() photoMode: boolean;
  @Input() buttonText: string;
  @Input() formControl: FormControl;
  @Input() imageUrl: string | ArrayBuffer;
  @Input() maxSize = 2 * 1024 * 1024; // default: 2MB

  @Output() onFileDropped = new EventEmitter<any>();
  @Output() bigFileDropped = new EventEmitter();

  @ViewChild('fileInput') fileInput: ElementRef;

  public icon = Icon;

  public isDragover: boolean;
  private previousImageUrl: string | ArrayBuffer;

  constructor(private cdr: ChangeDetectorRef) {

  }

  ngOnChanges() {
    if (this.imageUrl && !this.previousImageUrl && this.imageUrl !== '') {
      this.previousImageUrl = this.imageUrl;
    }
    if (this.imageUrl === '') {
      this.imageUrl = this.previousImageUrl;
    }
    this.cdr.detectChanges();
  }

  onDragOver(event): void {
    event.preventDefault();
    event.stopPropagation();
    this.isDragover = true;
    this.cdr.detectChanges();
  }

  onDragLeave(event): void {
    event.preventDefault();
    event.stopPropagation();
    this.isDragover = false;
    this.cdr.detectChanges();
  }

  onDrop(event): void {
    event.preventDefault();
    event.stopPropagation();
    this.isDragover = false;
    this.uploadFile(event.dataTransfer.files);
  }

  fileSelected(event){
    event.preventDefault();
    event.stopPropagation();

    this.uploadFile(event.target.files);
  }

  uploadFile(files: FileList) {
    if (!this.imageSizeCheck(files)) {
      this.bigFileDropped.emit();
      // At creation no imageURl is passed so the first onChanges hasn't been called before modification
      // But at first modification onChanges is called with the new value
      // in this case previousImageUrl === imageUrl so we pass it to null.
      this.imageUrl = this.previousImageUrl === this.imageUrl ? null : this.previousImageUrl;
      this.formControl.patchValue(this.imageUrl);
      this.previousImageUrl = this.imageUrl;
      return;
    }
    if (files[0]) {
      this.onFileDropped.emit(files);
      if (this.photoMode) {
        const reader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onload = event => {
          this.imageUrl = reader.result;
          this.previousImageUrl = this.imageUrl;
          this.formControl.patchValue(this.imageUrl);
          this.cdr.detectChanges();
        };
      }
    }
  }

  imageSizeCheck(files: FileList) {
    return !files[0] || files[0].size < this.maxSize;
  }

  openExplorer() {
    this.fileInput.nativeElement.click();
  }
}
