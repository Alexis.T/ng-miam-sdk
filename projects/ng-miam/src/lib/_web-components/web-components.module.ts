import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecipeCardComponent } from './recipe-card/recipe-card.component';
import { BasketPreviewBlockComponent } from './basket-preview-block/basket-preview-block.component';
import { BasketPreviewLineComponent } from './basket-preview-line/basket-preview-line.component';
import { ComponentsModule } from '../_components/components.module';
import { ModalComponent } from './modal/modal.component';
import { LoaderComponent } from './loader/loader.component';
import { BasketPreviewDisabledComponent } from './basket-preview-disabled/basket-preview-disabled.component';
import { DrawerComponent } from './drawer/drawer.component';
import { OrderButtonComponent } from './order-button/order-button.component';
import { ReplaceItemComponent } from './replace-item/replace-item.component';
import { PosSelectionComponent } from './pos-selection/pos-selection.component';
import { PosCardComponent } from './pos-card/pos-card.component';
import { DragDropInputComponent } from './drag-drop-input/drag-drop-input.component';
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';
import { DisplayRecipeCardComponent } from './display-recipe-card/display-recipe-card.component';
import { FavoritePageComponent } from './favorite-page/favorite-page.component';
import { RecipeFormComponent } from './recipe-form/recipe-form.component';
import { PlannerPageComponent } from './planner/planner.component';
import { PlannerCalendarComponent } from './planner-calendar/planner-calendar.component';
import { PlannerSideMenuComponent } from './planner-side-menu/planner-side-menu.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CommonModule } from '@angular/common';
import { UtilsModule } from '../_utils/utils.module';
import { RecipeModalComponent } from './recipe-modal/recipe-modal.component';
import { RecipesHistoryComponent } from './recipes-history/recipes-history.component';
import { RecipeHelperComponent } from './recipe-helper/recipe-helper.component';
import { RecipeAddonComponent } from './recipe-addon/recipe-addon.component';
import { RecipeTagsComponent } from './recipe-tags/recipe-tags.component';
import { CatalogRecipeCardComponent } from './catalog-recipe-card/catalog-recipe-card.component';
import { RecipeCatalogComponent } from './recipe-catalog/recipe-catalog.component';
import { CatalogCategoryComponent } from './catalog-category/catalog-category.component';
import { CatalogListComponent } from './catalog-list/catalog-list.component';
import { CatalogHeaderComponent } from './catalog-header/catalog-header.component';
import { RecipeCardFromIdComponent } from './recipe-card-from-id/recipe-card-from-id.component';
import { PersonalRecipesComponent } from './personal-recipes/personal-recipes.component';
import { RecipeStepperComponent } from './recipe-stepper/recipe-stepper.component';


// TODO rename recipecard and displaycard
export const WEB_COMPONENTS = [
  RecipeCardComponent,
  BasketPreviewBlockComponent,
  BasketPreviewLineComponent,
  BasketPreviewDisabledComponent,
  ModalComponent,
  LoaderComponent,
  DrawerComponent,
  OrderButtonComponent,
  ReplaceItemComponent,
  PosSelectionComponent,
  PosCardComponent,
  DragDropInputComponent,
  RecipeDetailsComponent,
  DisplayRecipeCardComponent,
  RecipeFormComponent,
  FavoritePageComponent,
  PlannerPageComponent,
  PlannerCalendarComponent,
  PlannerSideMenuComponent,
  RecipeModalComponent,
  RecipesHistoryComponent,
  RecipeHelperComponent,
  RecipeAddonComponent,
  RecipeTagsComponent,
  CatalogRecipeCardComponent,
  RecipeCatalogComponent,
  CatalogCategoryComponent,
  CatalogListComponent,
  CatalogHeaderComponent,
  RecipeCardFromIdComponent,
  PersonalRecipesComponent,
  RecipeStepperComponent
];
// Tag names as imported in client application
// Warning : do not use caps, use dashed case only
export const WEB_COMPONENTS_NAMES = [
  'recipe-card',
  'basket-preview-block',
  'basket-preview-line',
  'basket-preview-disabled',
  'modal',
  'loader',
  'drawer',
  'order-button',
  'replace-item',
  'pos-selection',
  'pos-card',
  'drag-drop-input',
  'recipe-detail',
  'display-recipe-card',
  'recipe-form',
  'favorite-page',
  'planner',
  'planner-calendar',
  'planner-side-menu',
  'recipe-modal',
  'recipes-history',
  'recipe-helper',
  'recipe-addon',
  'recipe-tags',
  'catalog-recipe-card',
  'recipe-catalog',
  'catalog-category',
  'catalog-list',
  'catalog-header',
  'recipe-card-from-id',
  'personal-recipes',
  'recipe-stepper',
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    UtilsModule
  ],
  declarations: WEB_COMPONENTS,
  exports: WEB_COMPONENTS
})
export class WebComponentsModule {
}
