import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, Output, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { DocumentCollection } from 'ngx-jsonapi';
import { Subscription } from 'rxjs';
import { skipWhile } from 'rxjs/operators';
import { Recipe } from '../../_models';
import { RecipesService } from '../../_services';
import { Icon } from '../../_types/icon.enum';
import shuffle from 'lodash-es/shuffle';
import { CatalogRecipeCardComponent } from '../catalog-recipe-card/catalog-recipe-card.component';

// miam-catalog-recipe-card : width = 330px + miam-catalog-category__card : margin : 16px
// we include a margin in the card to get a space this is the card width + the margin
const CARD_WIDTH = 346;
const SPACE_BETWEEN_CARD = 16;
const SLIDER_WIDTH = 48;

@Component({
  selector: 'ng-miam-catalog-category',
  templateUrl: './catalog-category.component.html',
  styleUrls: ['./catalog-category.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CatalogCategoryComponent implements OnDestroy, OnChanges {
  @Input() filters: any; // Remotefilters for the request (ex: { cost: "3,eq", search:"pommes de terre"})
  @Input() title = '';
  @Output() displayList: EventEmitter<any>;

  @ViewChild('categoryContent') categoryContent: ElementRef;
  @ViewChild('categoryCards') categoryCards: ElementRef;
  @ViewChildren(CatalogRecipeCardComponent)  recipeCards!: QueryList<CatalogRecipeCardComponent>;

  subscriptions: Subscription[];
  recipes: Recipe[];
  isLoading: boolean;
  icon = Icon;

  recipeCardsTotalSize = 0; // native size of the card contain including overflow
  numberOfSections = 1;  // number of card group  that feet in recipeCardsTotalSize
  currentSection = 0; // index of the section show on screen
  nbCardsInAnExtremSections = 0; // nb of card that can fit in screen with one scroll button
  nbCardsInAMiddleSection = 0; // nb of card that can fit in screen with two scroll buttons
  categoryBandWidth = 0; // native size of visible card container scroll button

  constructor(
    private recipesService: RecipesService,
    private cdr: ChangeDetectorRef) {
    this.displayList = new EventEmitter();
    this.subscriptions = [];
    this.recipes = [];
    this.isLoading = true;
  }

  ngOnChanges(): void {
    if (this.filters) {
      this.loadRecipes();
    }
  }

  loadRecipes() {
    this.subscriptions.push(this.recipesService.all({
      page: { size: 20, number: 1 },
      remotefilter : this.filters,
      include: ['ingredients', 'recipe-steps', 'recipe-status', 'sponsors']
    }).pipe(
      skipWhile(resp => resp.is_loading)
    )
    .subscribe((result: DocumentCollection<Recipe>) => {
      // Shuffle the recipes so we don't have the same order everytime this category is shown
      this.recipes = shuffle(result.data);
      this.isLoading = false;
      this.cdr.detectChanges();
      this.createSections();
    }));
  }

  createSections() {
    this.recipeCardsTotalSize = this.categoryCards.nativeElement.scrollWidth; // approx number of recipes * CARD_WIDTH
    this.recalculateSections();
    this.cdr.detectChanges();
  }

  recalculateSections(){
    // the size of the window in which we put recipes and sliders
    this.categoryBandWidth = this.categoryContent.nativeElement.clientWidth;
    // at start we have only one slider, two in the midle so we may not be able to fit same number of cards
    this.nbCardsInAnExtremSections =  Math.floor((this.categoryBandWidth - SLIDER_WIDTH) / CARD_WIDTH);
    this.nbCardsInAMiddleSection = Math.floor((this.categoryBandWidth - 2 * SLIDER_WIDTH) / CARD_WIDTH);

    const totalWidthOfCardsInAnExtremSection = this.nbCardsInAnExtremSections * CARD_WIDTH;
    const totalWidthOfCardsmiddleSection = this.nbCardsInAMiddleSection * CARD_WIDTH;

    const onlyOneSection = this.categoryBandWidth >= this.recipeCardsTotalSize;
    const twoSections = totalWidthOfCardsInAnExtremSection * 2 >= this.recipeCardsTotalSize;
    if (onlyOneSection) {
      this.numberOfSections = 1;
    } else if (twoSections) {
      this.numberOfSections = 2;
    } else {
      // in the case we have at least the starting section the ending section and one or more middle section
      //  2 * cardsInAnExtremSections => nb cards in extrem section
      // this.recipeCardsTotalSize - 2 * cardsInAnExtremSections => nb card in middle section
      this.numberOfSections = 2 + (this.recipeCardsTotalSize - 2 * totalWidthOfCardsInAnExtremSection) / totalWidthOfCardsmiddleSection;
    }
  }

  xTranslation() {
    if (this.currentSection === 0){
      return 0;
    } else if (this.currentSection === (this.numberOfSections - 1)){
      // the card contain a space of SPACE_BETWEEN_CARD on the LEFT, we a a space at the end for display purpose
      return this.recipeCardsTotalSize - this.categoryBandWidth + SPACE_BETWEEN_CARD;
    }
    // we are in the middle
    const firstexternSectionWidth = this.nbCardsInAnExtremSections * CARD_WIDTH;
    const middleSectionWidth = (this.currentSection - 1) * this.nbCardsInAMiddleSection * CARD_WIDTH;
    return firstexternSectionWidth + middleSectionWidth - SLIDER_WIDTH ;
  }

  slideLeft() {
    // current section can be a float if screen is resized
    this.currentSection = Math.max(this.currentSection - 1, 0);
    this.cdr.detectChanges();
    this.sendEvents();
  }

  slideRight() {
    // current section can be a float if screen is resized
    this.currentSection = Math.min(this.currentSection + 1, this.numberOfSections - 1);
    this.cdr.detectChanges();
    this.sendEvents();
  }

  sendEvents() {
    // set a debounce to let the view update and recipe to be in viewport
    setTimeout(() => {
      // the send show event check if card is in view
      this.recipeCards.forEach( recipeCard => {
        recipeCard.sendShowEvent('show-recipe-card-catalog');
      });
    }, 1000);
  }

  /**
   * to call when component resized
   */
  updateWidth(){
    const oldNbOfCardsInAnExtremSection = this.nbCardsInAnExtremSections;
    const oldNbCardsInAMiddleSection = this.nbCardsInAMiddleSection;
    const currentSectionWasFirst = this.currentSection === 0;
    const currentSectionWasLast = this.currentSection === this.numberOfSections - 1;
    this.recalculateSections();
    if (currentSectionWasFirst) {
      this.currentSection = 0;
    } else if (currentSectionWasLast) {
      this.currentSection = this.numberOfSections - 1;
    } else {
      // we want oldCardsForFirstSection + oldCardsBySection * currentSection = cardsForFirstSection + cardsBtSection * new_currentSection
      // our goal is to keep the same card at the start of our visible div
      const firstSectionDelta = oldNbOfCardsInAnExtremSection - this.nbCardsInAnExtremSections;
      this.currentSection = (firstSectionDelta + this.currentSection * oldNbCardsInAMiddleSection) / this.nbCardsInAMiddleSection;
    }
    this.cdr.detectChanges();
  }

  displayListMode() {
    this.displayList.emit({title: this.title, filters: this.filters});
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
