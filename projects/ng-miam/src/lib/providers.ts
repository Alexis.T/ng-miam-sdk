import {InjectionToken} from "@angular/core";

export const MIAM_STATUS_ID = new InjectionToken<any>("MIAM_STATUS_ID");
export const MIAM_PROVIDER_ID = new InjectionToken<any>("MIAM_PROVIDER_ID");
export const MIAM_BASKET_PATH = new InjectionToken<any>("MIAM_BASKET_PATH");
