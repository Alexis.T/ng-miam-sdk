import { NgModule } from '@angular/core';
import { ReadableFloatNumberPipe, EllipsisPipe, ExtendedDatePipe, CapitalizeFirstLetterPipe, SafePipe } from './pipes';
import { AutowidthInputDirective } from './directives';


@NgModule({
  imports: [
  ],
  declarations: [
    ReadableFloatNumberPipe,
    EllipsisPipe,
    SafePipe,
    CapitalizeFirstLetterPipe,
    AutowidthInputDirective,
    ExtendedDatePipe
  ],
  exports: [
    ReadableFloatNumberPipe,
    EllipsisPipe,
    SafePipe,
    CapitalizeFirstLetterPipe,
    AutowidthInputDirective,
    ExtendedDatePipe
  ]
})
export class UtilsModule {
  constructor() {}
}
