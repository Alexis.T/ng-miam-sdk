import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import isWithinInterval from 'date-fns/isWithinInterval';
import startOfWeek from 'date-fns/startOfWeek';
import endOfWeek from 'date-fns/endOfWeek';
import subDays from 'date-fns/subDays'

@Pipe({
  name: 'extendedDate'
})
export class ExtendedDatePipe extends DatePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    if (this.isToday(value)) return `Aujourd'hui`;
    if (this.isYesterday(value)) return `Hier`;
    if (this.isLastWeek(value)) return super.transform(value, 'dddd') + ' dernier';

    return 'Le ' + super.transform(value, 'dd MMMM yyyy');

  }

  isToday(someDate) {
    const today = new Date();
    return this.isSameDay(today, someDate);
  }

  isYesterday(someDate) {
    const yesterday = subDays(new Date(), 1) ;
    return this.isSameDay(yesterday, someDate);
  }

  isSameDay(date1: Date, date2: Date) {
    return date1.getDate() === date2.getDate() &&
      date1.getMonth() === date2.getMonth() &&
      date1.getFullYear() === date2.getFullYear();
  }

  isLastWeek(someDate) {
    const today = new Date();
    return isWithinInterval(new Date(someDate), { start: subDays(startOfWeek(today), 7), end: subDays(endOfWeek(today), 7) });

  }
}
