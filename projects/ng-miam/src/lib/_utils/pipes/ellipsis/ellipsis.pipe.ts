import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {
  transform(val: string, args: number) {
    if (args === undefined ||  args >= val?.split(' ')?.length) {
      return val;
    }
    return val.split(' ').slice(0, args).join(' ')  + ' ...';
  }
}