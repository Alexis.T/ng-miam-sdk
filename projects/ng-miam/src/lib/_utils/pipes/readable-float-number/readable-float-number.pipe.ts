import { Pipe, PipeTransform } from '@angular/core';

const COUNTLESS_UNITS = ['un peu', 'un petit peu']; 

@Pipe({ name: 'readableFloatNumber' })
export class ReadableFloatNumberPipe implements PipeTransform {
  constructor() { }

  transform(value: string, unit?: string) {
    if (this.isUnitCountless(unit)) {
      return unit;
    }

    const valueToNumber = parseFloat(value);
    if (valueToNumber < 1) {
      unit = this.singularize(unit);
      return `${this.frac(valueToNumber)} ${unit ? unit : ''}`;
    } else if (valueToNumber === 1) {
      unit = this.singularize(unit);
    } else if (valueToNumber > 1) {
      unit = this.pluralize(unit);
    }

    if (Number.isInteger(valueToNumber)) {
      return `${valueToNumber} ${unit ? unit : ''}`;
    }
    return `${valueToNumber.toFixed(2)} ${unit ? unit : ''}`;
  }

  /**
   * Algo Faray to find aproximative fraction
   * @param value 
   * @param maxdenominator not mandatory default 10
   */
  private frac(value: number, maxdenominator: number = 10): string {
    let num1 = 0;
    let den1 = 1;
    let num2 = 1;
    let den2 = 1;
    while (den1 <= maxdenominator && den2 <= maxdenominator) {
      let mediant = (num1 + num2) / (den1 + den2)
      if (value == mediant) {
        if (den1 + den2 <= maxdenominator) {
          return this.render_frac(value, num1 + num2, den1 + den2);
        } else if (den2 > den1) {
          return this.render_frac(value, num2, den2);
        } else {
          return this.render_frac(value, num1, den1);
        }
      } else if (value > mediant) {
        num1 = num1 + num2;
        den1 = den1 + den2;
      } else {
        num2 = num1 + num2;
        den2 = den1 + den2;
      }
    }
    if (den1 > maxdenominator) {
      return this.render_frac(value, num2, den2);
    } else {
      return this.render_frac(value, num1, den1);
    }
  }

  private render_frac(original_value: number, num: number, denom: number): string {
    if (num === 0) {
      // if we can't convert original value in a decent fraction better not to convert it at all
      return `${original_value.toFixed(2)}`
    }
    return `${num}/${denom}`
  }

  // If the unit if big enough, it's "probably not" a regular unit (g, L, cL, etc.)
  // In this case, add/remove plural 's' to/from its first member (gramme-s, cuillère-s à café)
  // TODO : handle in back !!!
  private pluralize(unit): string {
    if (!unit || unit.length <= 3) {
      return unit;
    }

    const unitArray = unit.split(' ');
    unitArray[0] = unitArray[0].replace(/(.*[^s])s*$/, "$1s");
    return unitArray.join(' ');
  }

  private singularize(unit): string {
    if (!unit || unit.length <= 3) {
      return unit;
    }

    const unitArray = unit.split(' ');
    unitArray[0] = unitArray[0].replace(/(.*[^s])s*$/, "$1");
    return unitArray.join(' ');
  }

  private isUnitCountless(unit: string): boolean {
    return COUNTLESS_UNITS.indexOf(unit) > -1;
  }
}

