import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizeFirstLetter'
})
export class CapitalizeFirstLetterPipe implements PipeTransform {
  transform(val: string) {
    if (!val) { return ''; }
    return val.charAt(0).toUpperCase() + val.slice(1).toLowerCase();
  }
}
