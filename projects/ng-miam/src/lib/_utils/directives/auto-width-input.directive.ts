import {AfterViewInit, Directive, ElementRef, HostListener, Input, OnChanges, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[autoWidthInput]'
})
export class AutowidthInputDirective implements AfterViewInit{

  @Input() maxSize = 'none';

  constructor(private el: ElementRef) {}


  ngAfterViewInit(): void {
    this.resize();
  }

  @HostListener('keyup') onKeyUp() {
    this.resize();
  }

  @HostListener('focus') onFocus() {
    this.resize();
  }

  resize() {
    if (this.maxSize !== 'none' && this.el.nativeElement.value.length >= +this.maxSize ) {
      this.el.nativeElement.setAttribute('size', this.maxSize );
      return;
    }
    this.el.nativeElement.setAttribute('size', this.el.nativeElement.value ? this.el.nativeElement.value.length : 1 );
  }
};