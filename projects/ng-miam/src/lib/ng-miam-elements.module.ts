import { LOCALE_ID, NgModule } from '@angular/core';

import { NgxJsonapiModule } from 'ngx-jsonapi';
import {
  GroceriesListsService,
  RecipesService,
  IngredientsService,
  RecipeStepsService
} from './_services';

import { MIAM_BASKET_PATH } from './providers';
import { MiamInterceptor } from './_services/interceptor.service';
import { environment } from './environment';

import { Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { WebComponentsModule } from './_web-components/web-components.module';
import { WEB_COMPONENTS } from './_web-components/web-components.module';
import { WEB_COMPONENTS_NAMES } from './_web-components/web-components.module';
import { BasketsService } from './_services/baskets.service';
import { BasketEntriesService } from './_services/basket-entries.service';
import { GroceriesEntriesService } from './_services/groceries-entries.service';
import { ContextService } from './_services/context.service';
import { ItemsService } from './_services/items.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AnalyticsService } from './_services/analytics.service';
import { BrowserModule } from '@angular/platform-browser';

const MIAM_API_HOST = `${environment.miamAPI}/api/v1/`;
const MIAM_WEB_URL = `${environment.miamWeb}/fr/app`;
const MIAM_BASKET_URL = `${MIAM_WEB_URL}/mon-panier`;

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');

@NgModule({
  imports: [
    NgxJsonapiModule.forRoot({ url: MIAM_API_HOST }),
    WebComponentsModule,
    BrowserModule,
  ],
  providers: [
    GroceriesListsService,
    GroceriesEntriesService,
    BasketsService,
    BasketEntriesService,
    RecipesService,
    ItemsService,
    IngredientsService,
    RecipeStepsService,
    AnalyticsService,
    { provide: HTTP_INTERCEPTORS, useClass: MiamInterceptor, multi: true },
    // Load provider and status
    { provide: MIAM_BASKET_PATH, useValue: MIAM_BASKET_URL },
    {provide: LOCALE_ID, useValue: 'fr' }
  ],
  entryComponents: WEB_COMPONENTS
})
export class NgMiamElementsModule {
  // Create custom elements so the WebComponents can be interpreted by the browser regardless of the js framework used
  constructor(
    injector: Injector,
    context: ContextService
  ) {
    WEB_COMPONENTS.forEach((component, idx) => {
      const ngElement = createCustomElement(component, { injector });
      customElements.define(`webc-miam-${WEB_COMPONENTS_NAMES[idx]}`, ngElement);
    });
    this.injectModals();
  }

  ngDoBootstrap() {
  }

  // Inject RecipeDetailsModal component dynamically when lib starts
  private injectModals() {
    const modalDetails = document.createElement('webc-miam-recipe-modal');
    document.body.appendChild(modalDetails);
    const modalHelper = document.createElement('webc-miam-recipe-helper');
    document.body.appendChild(modalHelper);
  }
}
