import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Icon } from '../../_types/icon.enum';
import { normalize, parse, toString } from 'duration-fns';

@Component({
  selector: 'ng-miam-time-picker',
  templateUrl: './time-picker.component.html',
  styleUrls: ['./time-picker.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimePickerComponent implements OnChanges {

  @Input() icon: Icon;
  @Input() title: string;
  @Input() formControl: FormControl;

  public internFormGroup: FormGroup;

  constructor(private cdr: ChangeDetectorRef) {
    this.internFormGroup = new FormGroup({
      hours: new FormControl('00'),
      minutes: new FormControl('00'),
    });
  }

  ngOnChanges(): void {
    if (this.formControl && this.formControl.value) {
      const duration = normalize(parse(this.formControl.value));
      // convert days In hours
      if (duration.days) { duration.hours += duration.days * 24; }
      this.internFormGroup.patchValue({
         // max hours is 99
        hours: this.formatTwoDigits(Math.min(duration.hours, 99)),
        minutes:  this.formatTwoDigits(duration.minutes)
      });
      this.cdr.detectChanges();
    }
  }

  onBlur(): void {

    const currentMinutes = this.autoCorrect(+this.internFormGroup.value.minutes);
    const currentHours =  this.autoCorrect(+this.internFormGroup.value.hours);

    this.internFormGroup.patchValue({
      // max hours is 99
      hours: this.formatTwoDigits(Math.min(Math.floor(+currentMinutes / 60) + +currentHours, 99)),
      minutes: this.formatTwoDigits(+currentMinutes % 60)
    });
    this.updateForm();
    this.cdr.detectChanges();
  }

  autoCorrect(time: number){
    return isNaN(time) || time < 0 ? 0 : time;
  }

  formatTwoDigits(nb: number): string{
    return nb.toLocaleString('fr-FR', {
      minimumIntegerDigits: 2,
      useGrouping: false
    });
  }

  public updateForm() {
      const timeAsString = this.internFormGroup.getRawValue();
      const time = {hours: +timeAsString.hours, minutes: +timeAsString.minutes};
      const isoTime =  toString(normalize(time));
      this.formControl.patchValue(isoTime);
  }
}
