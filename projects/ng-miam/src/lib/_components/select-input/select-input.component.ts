import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectInputComponent  {

  @Input() placeholder: string;
  @Input() formControl: FormControl;
  @Input() options: any[];
  @Input() labelPath: string;

  icon = Icon;
  constructor() {
  }

  getNestedProperty(obj: any, propertyPath: Array<string>) {
    if (!(propertyPath?.length > 0)) { return obj; }
    return this.getNestedProperty(obj[`${propertyPath.shift()}`], propertyPath);
  }

  /** this function is use to compare object in option and selected option  */
  compareFn(obj1: any, obj2:any): boolean {
    return  obj1 === obj2 || obj1?.id === obj2?.id;
  }
}