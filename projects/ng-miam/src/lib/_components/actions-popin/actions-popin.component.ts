import { Component, ViewEncapsulation, ChangeDetectionStrategy, Output, ElementRef, HostListener, EventEmitter } from '@angular/core';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-actions-popin',
  templateUrl: './actions-popin.component.html',
  styleUrls: ['./actions-popin.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionsPopinComponent {
  @Output() close = new EventEmitter();
  @Output() actionTriggered = new EventEmitter<string>();
  icon = Icon;

  constructor(private eRef: ElementRef) {}

  @HostListener('document:click', ['$event']) onclick(e: Event): void {
    if (this.eRef?.nativeElement?.contains(e.target) === false) {
      this.close.emit();
    }
  }

  emit(action: string) {
    this.actionTriggered.emit(action);
    this.close.emit();
  }
}