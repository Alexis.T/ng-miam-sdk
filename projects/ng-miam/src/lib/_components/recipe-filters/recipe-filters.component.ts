import { Component, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter, OnInit, Input, OnChanges } from '@angular/core';
import { RecipeTypeService } from '../../_services';
import { Icon } from '../../_types/icon.enum';
import { RecipeFilters } from '../../_types/recipe-filters';


@Component({
  selector: 'ng-miam-recipe-filters',
  templateUrl: './recipe-filters.component.html',
  styleUrls: ['./recipe-filters.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeFiltersComponent implements OnChanges {
  @Input() filters: RecipeFilters;
  @Output() filterChanged = new EventEmitter<RecipeFilters>();
  @Output() filterReset = new EventEmitter<void>();
  @Output() filterCollapsed = new EventEmitter<void>();

  isFilterCollapsed = false;
  icon = Icon;

  constructor(
    private cdr: ChangeDetectorRef) { }

  ngOnChanges() {
    this.cdr.detectChanges();
  }

  // When filters are mutually exclusives, on click on one we uncheck all before model change
  uncheckFiltersOnChange(field: string) {
    this.filters[field].forEach(f => f.value = false);
  }

  toggleFilter(){
    this.isFilterCollapsed = !this.isFilterCollapsed;
    this.cdr.detectChanges();
    this.filterCollapsed.emit();
  }
}
