import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, SimpleChanges, ViewEncapsulation } from "@angular/core";
import { FormControl } from "@angular/forms";
import { RecipeTypeService } from "../../_services/recipe-type.service";
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-recipe-type-chooser',
  templateUrl: './recipe-type-chooser.component.html',
  styleUrls: ['./recipe-type-chooser.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeTypeChooserComponent implements OnChanges {

  @Input() formControl: FormControl;

  public icon = Icon;
  public choosedType: string;

  constructor(private cdr: ChangeDetectorRef, private recipeTypeService: RecipeTypeService ){
    this.choosedType = '';
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (Object.keys(changes).includes('formControl')){
      this.initTypeChooser();
      this.cdr.detectChanges();
    }
  }

  initTypeChooser(){
    if (this.formControl.value){
      this.choosedType = this.formControl.value.attributes.name;
    }else if (this.choosedType){
      this.formControl.patchValue(this.recipeTypeService.getTypeByName(this.choosedType));
      this.formControl.markAsDirty();
    }
  }

  choosed(type: string){
    this.formControl?.patchValue(this.recipeTypeService.getTypeByName(type));
    this.formControl?.markAsDirty();
    this.choosedType = type;
    this.cdr.detectChanges();
  }
}
