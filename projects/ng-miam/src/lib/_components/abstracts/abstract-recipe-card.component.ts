import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { forkJoin, Subscription } from 'rxjs';
import { Ingredient } from '../../_models';
import { Recipe } from '../../_models/recipe';
import { RecipesService, GroceriesListsService, PointOfSalesService, ContextService, RecipeEventsService, UserService } from '../../_services';
import { Skeleton } from '../../_types/skeleton.enum';
import { Icon } from '../../_types/icon.enum';
import { AnalyticsService } from '../../_services/analytics.service';
import { RecipeLike } from '../../_models/recipe-like';
import { stopEventPropagation } from '../../_utils';
import { take } from 'rxjs/operators';


@Component({ template: '' })
export abstract class AbstractRecipeCardComponent implements OnDestroy {
  @Input() headerText = '';
  @Output() hide: EventEmitter<void>;

  recipe: Recipe;
  hovered = false;
  ingredientsConcatName: string;
  icon = Icon;
  skeleton = Skeleton;
  protected analyticsEventSent = false;
  protected subscriptions: Subscription[];

  constructor(
    public cdr: ChangeDetectorRef,
    public recipeService: RecipesService,
    public recipeEventsService: RecipeEventsService,
    public groceriesListsService: GroceriesListsService,
    public userService: UserService,
    protected posService: PointOfSalesService,
    private contextService: ContextService,
    protected analyticsService: AnalyticsService,
    protected element: ElementRef
    ) {
    this.subscriptions = [];
    this.hide = new EventEmitter();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  addRecipe(display = true): void {
    forkJoin([
      this.userService.isLogged$.asObservable().pipe(take(1)),
      this.posService.isPosValid().pipe(take(1))
    ]).subscribe(res => {
        const isHookOk = this.contextService.hookCallback(res[0], res[1]);
      if (isHookOk) {
        this.addRecipeActionOK(display);
      } else {
        this.addRecipeActionKO();
      }
    });
  }

  addRecipeActionOK(display) {
    if (!this.groceriesListsService.list$.value.hasRecipe(this.recipe.id)){
      this.recipeEventsService.sendEvent(this.recipe, this.recipeEventsService.ACTION_ADDED)
    }
    this.groceriesListsService.appendRecipeToList(this.recipe.id, this.recipe.modifiedGuests);
    if (display) {
      this.recipeService.displayObject(this.recipe, { guests : +this.recipe.modifiedGuests, previewMode: true});
    }
  }

  addRecipeActionKO() {
    localStorage.setItem('_miam/cached-recipe', JSON.stringify(this.recipe));
  }

  toggleHelper() {
    this.recipeService.toggleHelper();
    this.cdr.detectChanges();
  }

  openCalendar(event: Event) {
    stopEventPropagation(event);
    // TODO implement
  }

  shareRecipe(event: Event) {
    stopEventPropagation(event);
    // TODO implement
  }

  protected initIngredientsString(): void {
    this.ingredientsConcatName = '';
    if (this?.recipe?.relationships) {
      this.recipe.relationships[`ingredients`].data.forEach((ingredient: Ingredient, i: number) => {
        this.ingredientsConcatName += '' + ingredient.attributes.name;
        this.ingredientsConcatName += (i + 1 === this.recipe.relationships[`ingredients`].data.length) ? '' : ', ';
      });
    }
  }

  private isInViewport() {
    const rect = this.element.nativeElement.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  // Send analytics event when :
  // - new recipe is to be displayed AND is already visible in viewport
  // OR
  // - user scrolls and recipe becomes visible in viewport
  public sendShowEvent(eventType = 'show-recipe-card') {
    if (!this.recipe) {
      return false;
    }
    if (!this.analyticsEventSent && this.isInViewport()) {
      this.analyticsService.sendEvent(eventType);
      this.recipeEventsService.sendEvent(this.recipe, this.recipeEventsService.ACTION_SHOWN)
      this.analyticsEventSent = true;
    }
  }
}
