import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnChanges, OnDestroy, Output,
  ViewEncapsulation } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Icon } from '../../_types/icon.enum';
import { Recipe } from '../../_models/recipe';
import { RecipeLike } from '../../_models/recipe-like';
import { RecipeLikesService } from '../../_services/recipe-likes.service';
import { stopEventPropagation } from '../../_utils';

@Component({
  selector: 'ng-miam-like-button',
  templateUrl: './like-button.component.html',
  styleUrls: ['./like-button.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LikeButtonComponent implements OnChanges, OnDestroy{

  @Input() primaryColor = 'var(--m-color-primary)';
  @Input() width = 16;
  @Input() height = 16;
  @Input() recipe: Recipe;
  @Output() wasToggled = new EventEmitter<RecipeLike>();
  public icon = Icon;
  recipeLike: RecipeLike;

  private subscriptions: Subscription[];

  constructor(
    private recipeLikesService: RecipeLikesService,
    private cdr: ChangeDetectorRef) {
      this.subscriptions = [];
  }

  ngOnChanges(): void {
    this.initRecipeLike();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.recipeLikesService.removeLikeFromActivesIfNecessary(this.recipe.id);
  }

  initRecipeLike() {
    if (this.recipe) {
      this.subscriptions.push(this.recipeLikesService.getLike(this.recipe.id).subscribe( like => {
        this.recipeLike = like;
        this.cdr.detectChanges();
      } ));
    }
  }

  toggleLike(event: Event) {
    stopEventPropagation(event);
    if (this.recipe) {
      if (!this.recipeLike.id) {
        this.recipeLike.addRelationship(this.recipe, 'recipe');
        this.recipeLike.isPast = false;
      } else {
        this.recipeLike.toggle();
        this.wasToggled.emit(this.recipeLike);
      }
      this.recipeLikesService.updateLike(this.recipe.id, this.recipeLike);
      this.cdr.detectChanges();
    }
  }
}