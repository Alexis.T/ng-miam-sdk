import { ChangeDetectionStrategy, ChangeDetectorRef, OnChanges, ViewEncapsulation } from '@angular/core';
import { Component, Input, ViewChild, ElementRef, OnDestroy, Output, EventEmitter} from '@angular/core';
import { stopEventPropagation } from '../../_utils';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Icon } from '../../_types/icon.enum';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'ng-miam-counter-input',
  templateUrl: './counter-input.component.html',
  styleUrls: ['./counter-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterInputComponent implements OnDestroy, OnChanges {

  @Input() counter = 1;
  @Input() minRange = 1;
  @Input() maxRange = 100;
  @Output() counterChange = new EventEmitter<number>();
  @Output() selected = new EventEmitter<boolean>();

  @ViewChild('counterInput') counterInput: ElementRef;

  private subscriptions: Array<Subscription> = [];
  icon = Icon;
  counterChanged: BehaviorSubject<number>;

  constructor(private cdr: ChangeDetectorRef) { }

  ngOnChanges() {
    if (!this.counterChanged) {
      this.counterChanged = new BehaviorSubject(this.counter);
      this.subscriptions.push(
        this.counterChanged
          .pipe(debounceTime(500))
          .subscribe(() => {
            this.counterChange.emit(this.counter);
          })
      );
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  increment(event: Event): void {
    stopEventPropagation(event);
    this.focus();
    if (this.counter < this.maxRange) {
      this.counter++;
      this.counterChanged.next(this.counter);
      this.cdr.detectChanges();
    }
  }

  decrement(event: Event): void {
    stopEventPropagation(event);
    this.focus();
    if (this.counter > this.minRange) {
      this.counter--;
      this.counterChanged.next(this.counter);
      this.cdr.detectChanges();
    }
  }

  prevent(event: Event): void {
    stopEventPropagation(event);
  }

  updateValue() {
    this.cdr.detectChanges();
    if (!Number.isInteger(+this.counter)){
      this.counter = this.counterChanged.value;
      this.cdr.detectChanges();
      return;
    }
    if (this.counter < this.minRange) {
      this.counter = this.minRange;
    } else if (this.counter > this.maxRange) {
      this.counter = this.maxRange;
    }
    this.counterChanged.next(this.counter);
    this.cdr.detectChanges();
  }

  private focus() {
    this.counterInput.nativeElement.focus();
  }
}
