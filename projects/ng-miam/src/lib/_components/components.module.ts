import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasketEntryComponent } from './basket-entry/basket-entry.component';
import { IconComponent } from './icon/icon.component';
import { CORSOverlayComponent } from './cors-overlay/cors-overlay.component';
import { ListInputComponent } from './list-input/list-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule} from '@angular/cdk/drag-drop';
import { TextFieldModule } from '@angular/cdk/text-field';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { SliderComponent } from './slider/slider.component';
import { TimePickerComponent } from './time-picker/time-picker.component';
import { CounterInputComponent } from './counter-input/counter-input.component';
import { LikeButtonComponent } from './like-button/like-button.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabBodyComponent } from './tabs/tab-body.component';
import { TabItemComponent } from './tabs/tab-item.component';
import { TabLabelComponent } from './tabs/tab-label.component';
import { DeleteConfirmButtonComponent } from './delete-confirm-button/delete-confirm-button.component';
import { RecipePricingComponent } from './recipe-pricing/recipe-pricing.component';
import { AddonLinkComponent } from './addon-link/addon-link.component';
import { RecipeFiltersComponent } from './recipe-filters/recipe-filters.component';
import { SelectInputComponent } from './select-input/select-input.component';
import { UtilsModule } from '../_utils/utils.module';
import { TextInputComponent } from '../_components/text-input/text-input.component';
import { RecipeTypeChooserComponent } from './recipe-type-chooser/recipe-type-chooser.component';
import { ActionsPopinComponent } from './actions-popin/actions-popin.component';
import { CardCreateRecipeComponent } from './card-create-recipe/card-create-recipe.component';


export const COMPONENTS = [
  BasketEntryComponent,
  IconComponent,
  CORSOverlayComponent,
  ListInputComponent,
  SkeletonComponent,
  SliderComponent,
  TimePickerComponent,
  CounterInputComponent,
  LikeButtonComponent,
  TabsComponent,
  TabBodyComponent,
  TabItemComponent,
  TabLabelComponent,
  DeleteConfirmButtonComponent,
  RecipePricingComponent,
  AddonLinkComponent,
  RecipeFiltersComponent,
  SelectInputComponent,
  TextInputComponent,
  ActionsPopinComponent,
  CardCreateRecipeComponent,
  RecipeTypeChooserComponent,
  ActionsPopinComponent
];

@NgModule({
  imports: [
    UtilsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    TextFieldModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ComponentsModule {
}
