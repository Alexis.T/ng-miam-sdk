import { Component, Input, Output, EventEmitter, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, OnChanges } from '@angular/core';
import { stopEventPropagation } from '../../_utils';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-delete-confirm-button',
  templateUrl: './delete-confirm-button.component.html',
  styleUrls: ['./delete-confirm-button.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteConfirmButtonComponent implements OnChanges {
  public icon = Icon;

  @Output() deleteSIG = new EventEmitter<void>();

  isInComfirmStep: boolean;

  constructor(private cdr: ChangeDetectorRef) {
    this.deleteSIG = new EventEmitter();
  }
  ngOnChanges(): void {
    this.cdr.detectChanges();
  }

  remove() {
    this.deleteSIG.emit();
    this.isInComfirmStep = false;
    this.cdr.detectChanges();
  }

  cancel(event: Event) {
    stopEventPropagation(event);
    this.isInComfirmStep = false;
    this.cdr.detectChanges();
  }

  toogleConfirmStep(value: boolean) {
    this.isInComfirmStep = value;
    this.cdr.detectChanges();
  }
}
