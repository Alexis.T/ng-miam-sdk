import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-card-create-recipe',
  templateUrl: './card-create-recipe.component.html',
  styleUrls: ['./card-create-recipe.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardCreateRecipeComponent {
  @Output() clicked = new EventEmitter();
  icon = Icon;

  constructor() {}

}