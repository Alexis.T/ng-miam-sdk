import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, OnChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SliderComponent implements OnChanges {
  @Input() steps: string[];
  @Input() icons: Icon[];
  @Input() formControl: FormControl;

  @ViewChild('miamLeftOfSliderTrack') trackLeft: ElementRef;
  @ViewChild('miamSliderColored') coloredTrack: ElementRef;
  @ViewChild('miamSliderHandle') sliderHandle: ElementRef;

  public selectedIndex = 0;
  public width = 0;
  public showPogress = true;

  constructor(private cdr: ChangeDetectorRef) {
  }

  ngOnChanges(): void {
    if (this.formControl?.value) {
      // in db value are between 1 and 3
      const initialIndex = this.formControl.value - 1;
      this.selectedIndex =  initialIndex >= 0 &&  initialIndex < 3 ? initialIndex : 0;
      this.cdr.detectChanges();
      this.colorTrack();
    }
  }

  colorTrack() {
    this.coloredTrack.nativeElement.style.width = `${this.getDistanceBetweenHandleAndTrackLeft()}px`;
    this.cdr.detectChanges();
  }

  /**
   * @returns the distance between the x position of the middle of the handle and the left of the track
   */
  getDistanceBetweenHandleAndTrackLeft() {
    const preview = document.getElementsByClassName('miam-slider-input__track__preview')[0];
    const trackLeftPosition = this.trackLeft.nativeElement.getBoundingClientRect();
    // When we are dragging the handle, the handle in itself disappear and what is shown is a preview of it
    // So when we are dragging the handle, we need to check the position of the preview
    const handlePosition = preview ? preview.getBoundingClientRect() :
                            this.sliderHandle.nativeElement.getBoundingClientRect();

    return (handlePosition.left + handlePosition.width / 2) - trackLeftPosition.left;
  }

  selectStepAt(index){
    if (index === this.selectedIndex) {
      return;
    }
    this.selectedIndex = index;
    this.formControl.patchValue(index + 1);
    this.formControl.markAsDirty();
    this.cdr.detectChanges();
    this.colorTrack();
  }

}
