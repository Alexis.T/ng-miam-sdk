import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { FormControl} from '@angular/forms';
import { Icon } from '../../_types/icon.enum';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'ng-miam-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextInputComponent {

  @Input() icon: Icon;
  @Input() placeholder: string;
  @Input() formControl: FormControl;

  uniqueId;

  constructor() {
    this.uniqueId = uuidv4();
  }
}