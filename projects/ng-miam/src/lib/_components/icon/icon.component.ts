import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges } from '@angular/core';
import { ContextService } from '../../_services/context.service';
import { Icon } from '../../_types/icon.enum';

@Component({
    selector: 'ng-miam-icon',
    templateUrl: './icon.component.html',
    styleUrls: ['./icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent implements OnChanges {

    public icon = Icon;
    public iconOverrideUrl: string;

    @Input()
    primaryColor = 'black';

    @Input()
    secondaryColor = 'grey';

    @Input()
    width = 16;

    @Input()
    height = 16;

    @Input()
    iconName: Icon;

    constructor(private contextService : ContextService , private cdr :ChangeDetectorRef ) {
    }

    ngOnChanges(): void {
        this.iconOverrideUrl = this.contextService.getOverrideIconUrl(this.iconName);
        this.cdr.detectChanges();
    }

}
