import { ApplicationRef, ComponentFactoryResolver, EmbeddedViewRef, Injector, LOCALE_ID, NgModule } from '@angular/core';
import { ComponentsModule } from './_components/components.module';
import { NgxJsonapiModule } from 'ngx-jsonapi';
import { GroceriesListsService } from './_services';

import { RecipesService } from './_services/recipes.service';
import { RecipeLikesService } from './_services/recipe-likes.service';
import { BasketsService } from './_services/baskets.service';
import { MIAM_BASKET_PATH } from './providers';
import { environment } from './environment';
import { WebComponentsModule } from './_web-components/web-components.module';
import { BasketEntriesService } from './_services/basket-entries.service';
import { GroceriesEntriesService } from './_services/groceries-entries.service';
import { ContextService } from './_services/context.service';
import { ItemsService } from './_services/items.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MiamInterceptor } from './_services/interceptor.service';
import { AnalyticsService } from './_services/analytics.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RecipeModalComponent, RecipeHelperComponent } from './_web-components';

const MIAM_API_HOST = `${environment.miamAPI}/api/v1/`;
const MIAM_WEB_URL = `${environment.miamWeb}/fr/app`;
const MIAM_BASKET_URL = `${MIAM_WEB_URL}/mon-panier`;

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');


// @dynamic
@NgModule({
  imports: [
    NgxJsonapiModule.forRoot({url: MIAM_API_HOST}),
    ComponentsModule,
    WebComponentsModule,
    BrowserAnimationsModule,
  ],
  providers: [
    GroceriesListsService,
    GroceriesEntriesService,
    RecipesService,
    RecipeLikesService,
    BasketsService,
    BasketEntriesService,
    ItemsService,
    { provide: HTTP_INTERCEPTORS, useClass: MiamInterceptor, multi: true },
    AnalyticsService,
    { provide: MIAM_BASKET_PATH, useValue: MIAM_BASKET_URL },
    {provide: LOCALE_ID, useValue: 'fr' }
  ],
  exports: [
    ComponentsModule,
    WebComponentsModule
  ]
})
export class NgMiamModule {
  constructor(
    private context: ContextService,
    private factoryResolver: ComponentFactoryResolver,
    private injector: Injector,
    private appRef: ApplicationRef
  ) {
    this.injectModals();
  }

  // Inject RecipeDetailsModal component dynamically when lib starts
  private injectModals() {
    // Instantiate RecipeDetailsModalComponent & attach it to root view ref
    const factoryRecipeModal = this.factoryResolver.resolveComponentFactory(RecipeModalComponent);
    const componentRefRecipeModal = factoryRecipeModal.create(this.injector);
    this.appRef.attachView(componentRefRecipeModal.hostView);

    // Instantiate RecipeHelperComponent & attach it to root view ref
    const factoryHelper = this.factoryResolver.resolveComponentFactory(RecipeHelperComponent);
    const componentRefHelper = factoryHelper.create(this.injector);
    this.appRef.attachView(componentRefHelper.hostView);

    // Append corresponding elements to DOM
    const rootDomElemRecipeModal = (componentRefRecipeModal.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.body.appendChild(rootDomElemRecipeModal);
    const rootDomElemHelper = (componentRefHelper.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.body.appendChild(rootDomElemHelper);
  }

}
