export interface RecipeFilter {
  name: string;
  value: boolean;
  text: string;
}

export interface RecipeFilters {
  // type?: RecipeFilter[];
  difficulty?: RecipeFilter[];
  cost?: RecipeFilter[];
  time?: RecipeFilter[];
  search?: string;
  additionalFilters?: { filters: object, title: string };
}
